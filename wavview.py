#!/usr/bin/env python

"""
Displays wavfile (first parameter) in pyplot graph
@author: curt
@created: 4/20/15
"""

import math
import os
import sys

import numpy
from matplotlib import pyplot

from fishguts import log
from fishguts.audio import afile

if len(sys.argv)<2:
	print "usage: wavview.py <filename>"
	exit(1)

try:
	fpath=os.path.abspath(sys.argv[1])
	srate, samples=afile.read(fpath)
	normalized=numpy.asfarray(samples)
	# normalize
	if samples.dtype in (numpy.int16, numpy.uint16):
		normalized/=0x8000
	elif samples.dtype in (numpy.int32, numpy.uint32):
		normalized/=0x80000000
	vmax=max(abs(normalized.min()), normalized.max())
	vmax=min(1.0, math.ceil(vmax*10)/10)
	# create timeline in seconds
	seconds=numpy.arange(samples.size)/float(srate)
	pyplot.ylim((-vmax, vmax))
	pyplot.plot(seconds, normalized)
	pyplot.show()
except Exception as exception:
	log.write_failed(exception)
