"""
created: 5/9/15
@author: curt
"""
import sys

def parse(min_position=0, supported_options_single=None, supported_options_pair=None):
	"""
	Parses sys.argv and returns tuple of (options, positions).
	The options are stripped of their '-' so -d 10 would be returned as {"d":10}
	@param min_position: count
	@param supported_options_single: array of standalone option names: ['v', 'i']
	@param supported_options_pair: array of options that expect a pair:
	@return (options, positions)
	@throws exception if option is not matched or known or too few positions
	"""
	options={}
	positions=[]
	offset=1
	count=len(sys.argv)

	if supported_options_single is None:
		supported_options_single=[]
	if supported_options_pair is None:
		supported_options_pair=[]
	while offset<count:
		if sys.argv[offset].startswith("-"):
			option=sys.argv[offset][1:]
			if option in supported_options_single:
				options[option]=True
				offset+=1
			elif option in supported_options_pair:
				options[option]=sys.argv[offset+1]
				offset+=2
			else:
				raise Exception("unknown option '%s'"%option)
		else:
			positions.append(sys.argv[offset])
			offset+=1
	if len(positions)<min_position:
		raise Exception("missing params")
	return options, positions
