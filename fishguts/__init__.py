"""
created: 5/9/15
@author: curt
"""
import json
import math
import random

from pydash import collections
from pydash import objects
from pydash import predicates


#--- Functional handlers and asserts ---#
def raise_if_true(condition, text, exception=Exception):
	if condition:
		raise exception(text)

def raise_if_false(condition, text, exception=Exception):
	if not condition:
		raise exception(text)

def raise_if_none(condition, text, exception=Exception):
	if condition is None:
		raise exception(text)


#--- Functional enhancements to pydash. Most commonly that pydash does not return a collection for None collections ---#
def each(collection, callback=None):
	return collections.each(collection, callback) if collection is not None else []

def each_right(collection, callback=None):
	return collections.each_right(collection, callback) if collection is not None else []

def get(obj, path, default=None, required=False, error=""):
	"""
	pydash's algorithm is broken and can throw exceptions with incomplete paths (attempts to dereference default)
	@param obj: object to process
	@param path: path to dereference
	@param default: default value
	@param required: is it required? If so then raise a ValueError
	@param error: error text to raise
	@return: value
	"""
	try:
		v=objects.get(obj, path, default)
		if required and v is None:
			raise Exception()
		return v
	except:
		if required:
			raise ValueError(error if error else "'{0}' not found".format(path))
		else:
			return default


#--- generators ---#
def iloop(iterable):
	"""
	Generator that loops sequentially around and around until the end of time
	@param iterable: anything that supports array indexing
	"""
	raise_if_true(len(iterable)==0, "iterable must not be empty", ValueError)
	index=0
	length=len(iterable)
	while True:
		yield iterable[index%length]
		index+=1

def ibounce(iterable):
	"""
	Generator that bounces back and forth like a triangle wave between and including the first and last element until the end of time
	@param iterable: anything that supports array indexing
	"""
	raise_if_true(len(iterable)==0, "iterable must not be empty", ValueError)
	index=0
	increment=1
	length=len(iterable)
	while True:
		yield iterable[index]
		if length>1:
			index+=increment
			if index==length-1:
				increment=-1
			elif index==0:
				increment=1

def ialternate(iterable, offset=0):
	"""
	Generator that alternates from end to end and works it's way to the other side and keeps going:
	eg. [1,2,3,5] => 1, 5, 2, 3, 2, 3, 1, 5
	@param iterable: anything that supports array indexing
	@param offset: {int|"first"|"middle"|"last"}
	"""
	raise_if_true(len(iterable)==0, "iterable must not be empty", ValueError)
	length=len(iterable)
	increment=1
	if offset=="first":
		index=0
	elif offset=="middle":
		index=length/2
	elif offset=="last":
		# if we are starting that the end then we assume they want to work their way backwards
		# otherwise we will return the end nodes and wrap around to the beginning. If this is what
		# a caller wants then be explicit with the offset.
		index=length-1
		increment=-1
	else:
		index=int(offset)
	while True:
		offset=index%length
		yield iterable[offset]
		yield iterable[0-offset-1]
		index+=increment

def irandom(iterable):
	"""
	Generator that selects indexes randomly
	@param iterable: anything that supports array indexing
	"""
	raise_if_true(len(iterable)==0, "iterable must not be empty", ValueError)
	length=len(iterable)
	while True:
		yield iterable[random.randint(0, length-1)]


def isin(divisions, vfrom=-1.0, vto=1.0, count=None, bounce=False, phase=0.0):
	"""
	Generator that divides a sin cycle up into 'divisons' divisions
	@param divisions: number of divisions to break a period into
	@param vfrom: value at 270
	@param vto: value at 90
	@param count: number of times to iterate. defaults to None
	@param bounce: whether to bounce as in ibounce as opposed to loop
	@param phase: radian angle offset
	"""
	raise_if_true(divisions<1, "division must be > 0", ValueError)
	iindex=ibounce(range(divisions)) if bounce else iloop(range(divisions))
	factor=(vto-vfrom)/2.0
	while count is None or count>0:
		result=math.sin(phase+2*math.pi*iindex.next()/divisions)
		yield vfrom+(result+1)*factor
		if predicates.is_number(count):
			count-=1


def iexp(divisions, vfrom=0.0, vto=1.0, count=None, bounce=False, power=2.0):
	"""
	Generator that divides an exponential range into 'divisons' divisions
	@param divisions: number of divisions to break a period into
	@param vfrom: value at div[0]
	@param vto: value at div[-1]
	@param count: number of times to iterate. defaults to None
	@param bounce: whether to bounce as in ibounce as opposed to loop
	@param power: driving exponential curve
	"""
	raise_if_true(divisions<2, "division must be > 1", ValueError)
	iindex=ibounce(range(divisions)) if bounce else iloop(range(divisions))
	factor=float(vto-vfrom)
	while count is None or count>0:
		result=math.pow(iindex.next()/float(divisions-1), power)
		yield vfrom+result*factor
		if predicates.is_number(count):
			count-=1


#--- Functional additions ---#
def scrub(value, recurse=False, empty=(None, {})):
	"""
	remove "empty" dict values from value
	@param value: object to act upon
	@param recurse: whether to recurse or not
	@param empty: values that are considered empty and removable
	@return: object
	"""
	try:
		for key in value.keys():
			if recurse:
				scrub(value[key], recurse, empty)
			if collections.contains(empty, value[key]):
				del value[key]
	except:
		pass
	return value


#--- file utils ---#
def path_to_json(path_or_file):
	"""
	loads json from path or file and ensures that file is closed if it was successfully opened
	@param path_or_file: absolute/relative path or file object
	@return: dict if successful and None if attempt fails
	@raise: raises exception if attempt fails
	"""
	close=None
	try:
		if isinstance(path_or_file, file):
			j=json.load(path_or_file)
		else:
			close=open(path_or_file, "r")
			j=json.load(close)
		return j
	finally:
		if close is not None:
			close.close()

def json_to_path(path_or_file, data):
	"""
	saves data object in json format and closes file once done if the file was opened
	@param path_or_file: absolute/relative path or file object
	@param data: object that may be encoded by json encoder
	@raise: raises exception if attempt fails
	"""
	close=None
	try:
		if isinstance(path_or_file, file):
			json.dump(data, path_or_file)
		else:
			close=open(path_or_file, "w")
			json.dump(data, close)
	finally:
		if close is not None:
			close.close()

def path_to_name(path_or_file):
	"""
	to be consistent with the guys up above since we abstract the difference between file and str path
	@param path_or_file: absolute/relative path or file object
	@return: path or file.name depending on type
	"""
	if isinstance(path_or_file, file):
		return path_or_file.name
	else:
		return path_or_file
