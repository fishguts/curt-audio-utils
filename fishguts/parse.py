"""
created: 10/13/16
@author: curt
"""
import pydash

def to_bool(text):
	if type(text) is not str:
		return False
	return pydash.contains(("true", "t", "1"), text.lower())
