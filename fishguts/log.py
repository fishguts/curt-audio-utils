"""
created: 5/22/15
@author: curt
"""

_verbose=0

def set_verbose(verbose, very=False):
	global _verbose
	_verbose=2 if very else 1 if verbose else 0

def write_verbose(text, *args):
	if _verbose>0:
		print str(text).format(*args)

def write_very_verbose(text, *args):
	if _verbose>1:
		print str(text).format(*args)

def write_debug(text, *args):
	print ("debug: " + str(text)).format(*args)

def write_info(text, *args):
	print ("info: " + str(text)).format(*args)

def write_warn(text, *args):
	print ("warn: " + str(text)).format(*args)

def write_error(text, *args):
	print ("error: " + str(text)).format(*args)

def write_failed(text, *args):
	print ("failed: " + str(text)).format(*args)

def write_warn_if_false(condition, text, *args):
	if not condition:
		write_warn(text, *args)

def write_error_false(condition, text, *args):
	if not condition:
		write_error(text, *args)


