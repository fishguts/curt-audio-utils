"""
created: 5/23/15
@author: curt

Collection of functions that deliver information
"""

import fishguts
from pydash import predicates

#--- public api ---#
def get_zero_crossings(samples, interval=1, slope=True):
	"""
	Finds zero crossings and encodes them as follows: rising=+index, descending=-index
	@param samples: array of samples
	@param interval: min sample (or samples) interval between zero crossings
	@param slope: whether to include the sign as indication of rising or falling
	@return: list of zero crossings: rising=+index, descending=-index
	"""
	result=[]
	sample_offset=0
	icrossing=1 if slope else 0
	intervals=fishguts.iloop(interval if predicates.is_iterable(interval) else (interval,))
	try:
		while True:
			crossing=_get_next_zero_crossing(samples, sample_offset)
			result.append(crossing[icrossing])
			sample_offset=max(crossing[0]+1, sample_offset+intervals.next())
	except EOFError:
		return result


def get_rythmic_zero_crossings(samples, bars):
	"""
	Finds series of zero crossings as described by bars
	@param samples: array of samples
	@param bars: list of bars: { length:int, rhythm:[{length:int, offset:int|None}] }
	@return: { bars:[], flat:[] }
	"""
	result={
		"bars": [],
		"flat": []
	}
	sample_offset=0
	try:
		while True:
			for bar in bars:
				crossing_start=_get_next_zero_crossing(samples, sample_offset)
				sample_offset=crossing_start[0]
				data={
					"ref": bar,
					"start": sample_offset,
					"rhythm": []
				}
				for rhythmic_item in bar["rhythm"]:
					# note: both 'offset' and 'advance' may be negative and we don't mind but we do care about indexes
					# falling behind the 0 index since python indexing wraps around to the end...but we don't
					crossing_bar_start=_get_next_zero_crossing(samples, max(0, sample_offset+rhythmic_item.get("offset", 0)))
					crossing_bar_end=_get_next_zero_crossing(samples, crossing_bar_start[0]+rhythmic_item["length"])
					data["rhythm"].append({
						"ref": rhythmic_item,
						"start": crossing_bar_start[0],
						"length": crossing_bar_end[0]-crossing_bar_start[0],
						"crossing": crossing_bar_start[1],
					})
					result["flat"].append(data["rhythm"][-1])
					sample_offset+=max(0-sample_offset, rhythmic_item.get("advance", 0))
				result["bars"].append(data)
				if "length" in bar:
					# length trumps rhythm item's "advance" property which totally gets tossed out at the bar level.
					sample_offset=_get_next_zero_crossing(samples, crossing_start[0]+bar["length"])[0]
	except EOFError:
		return result



#--- private api ---#
def _get_next_zero_crossing(samples, offset):
	for isample in xrange(offset, len(samples)-1):
		sample_cur=samples[isample]
		sample_nxt=samples[isample+1]
		# note: if both are zero then at least these two samples don't have a direction.
		# We could seek back and be more aggressive. Let's see where it has consequences.
		if sample_cur<=0<=sample_nxt:
			return (isample, isample)
		elif sample_cur>=0>=sample_nxt:
			return (isample, -isample)
	raise EOFError()

