"""
created: 5/24/15
@author: curt

Pretty basic audio signal generator
"""

import numpy

#--- constants ---#
pi2=numpy.pi*2


#--- public api ---#
# noinspection PyUnresolvedReferences
def create_sin_normal(srate, freq, tlen=None, slen=None):
	"""
	creates a sine wave
	@param srate: sample rate
	@param freq: frequency
	@param tlen: length in time|seconds
	@param slen: length in samples
	@return: normalized numpy array
	"""
	tlen=tlen if tlen is not None else float(slen)/float(srate)
	if tlen<=0:
		return numpy.empty(0)
	cycles=freq*tlen
	result=numpy.sin(numpy.arange(0, pi2*cycles, (pi2*cycles)/(srate*tlen)))
	# edge case in which floating point arithmetic of our step vs. our stop renders 1 too many or too few samples
	if slen is not None:
		if slen!=result.size:
			result.resize(slen, refcheck=False)
	return result

def create_sin_samples(srate, freq, tlen=None, slen=None, amp=16000):
	"""
	creates a sine wave
	@param srate: sample rate
	@param freq: frequency
	@param tlen: length in time|seconds
	@param slen: length in samples
	@param amp: amplitude in 16bit sample value
	@return: normalized numpy array
	"""
	return numpy.array(create_sin_normal(srate, freq, tlen, slen)*amp, dtype=numpy.int16)

