"""
created: 5/22/15
@author: curt
"""

import numpy
import fishguts

def millis_to_samples(srate, millis):
	return int(numpy.ceil((srate*millis)/1000.0))

def seconds_to_samples(srate, seconds):
	return int(numpy.ceil(srate*seconds))

def samples_to_millis(srate, samples):
	return float(samples*1000)/srate

def samples_to_seconds(srate, samples):
	return float(samples)/srate

def scale_samples(samples, scalar, ioffset=0):
	iend=ioffset+len(scalar)
	samples[ioffset:iend]=(samples[ioffset:iend]*scalar).astype(samples.dtype)

def copy_samples(samples, ioffset=0, count=None, algorithm=None):
	"""
	copy range of samples into a newly created buffer
	@param samples: array of samples
	@param ioffset: offset to start copying from in samples
	@param count: total number of samples to copy
	@param algorithm:
		"zero": if not enough source samples then fill the trailing samples with zero
		"wrap": if not enough source samples then wrap until target buffer is filled
	@return:
	"""
	fishguts.raise_if_true(len(samples)==0, "buffer is empty")
	if count is None:
		count=len(samples)
	trailing=(len(samples)-ioffset)
	if count<=trailing:
		result=numpy.copy(samples[ioffset:ioffset+count])
	elif algorithm=="zero":
		result=numpy.zeros((count,), dtype=samples.dtype)
		result[:trailing]=samples[ioffset:]
	elif algorithm=="wrap":
		ioffset_target=0
		result=numpy.empty((count,), dtype=samples.dtype)
		while ioffset_target<count:
			result[ioffset_target:ioffset_target+trailing]=samples[ioffset:ioffset+trailing]
			ioffset=0
			ioffset_target+=trailing
			trailing=min(len(samples), count-ioffset_target)
	else:
		fishguts.raise_if_false(algorithm is None, "unknown algorithm")
		result=numpy.copy(samples[ioffset:ioffset+trailing])
	return result

