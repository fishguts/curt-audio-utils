"""
created: 8/29/15
@author: curt
"""
import fishguts
import numpy
import pydash
from fishguts.audio import asignal


def generate_stream(srate, data):
	"""
	Generates a normalized array of sample values
	@param srate: sample rate
	@param data: object with the following properties
		- sequence: iterable collection of frequencies to generate
		- srhythm: iterable collection of sample lengths to use to generate frequencies: [on, off, on, off...]
		- sattack: iterable collection of attack durations in samples
		- sdecay: iterable collection of decay durations in samples
	@return: numpy.array of fsamples
	"""
	sequence, sattacks, sdecays, rhythms, generator=_preprocess_data(data)
	slen=_estimate_length(sequence, rhythms)
	soffset=0
	rindex=0
	# build sample buffer
	result=numpy.zeros(slen, dtype=numpy.float)
	for sindex in xrange(0, len(sequence)):
		freq=sequence[sindex]
		# pick up all "offs" and adjust soffset accordingly
		soff, rindex=_get_length_while_rhythm_state(rhythms, "off", rindex)
		son=max(0, rhythms[rindex]["on"])
		soffset=max(0, soff+soffset)
		rindex=(rindex+1)%len(rhythms)
		sattack=min(son, sattacks[sindex])
		sdecay=min(son, sdecays[sindex])
		samples=generator(srate=srate, freq=freq, slen=son)
		assert(samples.size==son)
		# apply attack and decay
		samples[0:sattack]*=numpy.arange(0, sattack)/float(sattack)
		if sdecay>0:
			samples[-sdecay:]*=numpy.arange(sdecay, 0, -1)/float(sdecay)
		# add target elements in result. Size check is legacy but keeping around to maintain bulletproof'ness
		if result.size<soffset+son:
			result.resize((soffset+son,), refcheck=False)
		result[soffset:soffset+son]+=samples
		soffset+=son
	return result


#--- private API ---#
def _preprocess_data(data):
	generator=pydash.get(data, "generator", asignal.create_sin_normal)
	sequence=data["sequence"]
	sattacks=data["sattack"]
	sdecays=data["sdecay"]
	if "srhythm" in data:
		rhythms=_length_list_to_rhythm(data["srhythm"])
	else:
		rhythms=data["rhythm"]
	fishguts.raise_if_false(len(sequence)==len(sattacks)==len(sdecays), "sequencing integrity error")
	fishguts.raise_if_false(pydash.any_(rhythms, lambda  rhythm: "on" in rhythm), "rhythm must have at least one 'on' duration")
	return sequence, sattacks, sdecays, rhythms, generator

def _estimate_length(sequence, rhythms):
	slen=0
	rindex=0
	for _ in sequence:
		soff, rindex=_get_length_while_rhythm_state(rhythms, "off", rindex)
		slen=max(0, slen+soff)
		slen+=max(0, rhythms[rindex]["on"])
		rindex=(rindex+1)%len(rhythms)
	return slen

def _length_list_to_rhythm(sequence):
	"""
	translates an interable sequence into a rhythm sequence that may be used by generate_stream
	@param sequence: iterable of numbers
	@return:
	"""
	return [ {"off":sequence[index]} if index%2 else {"on":sequence[index]} for index in range(0, len(sequence))]

def _get_length_while_rhythm_state(rhythms, state, index=0):
	"""
	Sums the sample count of consecutive rhythms with state==state
	@param rhythms:
	@param state:
	@param index:
	@return: sample_length, rhythm_index of next state
	"""
	slen=0
	while True:
		rhythm=rhythms[index]
		if state not in rhythm:
			break
		slen+=rhythm[state]
		index=(index+1)%len(rhythms)
	return slen, index
