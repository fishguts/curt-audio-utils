"""
created: 5/24/15
@author: curt

Collection of audio transforms
"""

import numpy

def modulate(samples, scalar, rate=1.0):
	"""
	Modulates samples by scalar at the rate specified by rate
	@param samples: numpy array of samples of any bit width.
	@param scalar: normalized array of values of any length which will be used to modify samples
	@param rate: normalized rate at which scalar should be applied
		rate=0.5 => 2 samples per scalar (interpolated)
		rate=1 => 1 sample per scalar
		rate=2 => 1 sample per 2 scalars
	@return: new modulated numpy array of samples length
	"""
	sample_count=len(samples)
	scalar_indexes=numpy.arange(len(scalar))
	# these are the interpolated indexes into scalar of which we want 1 for every sample
	# note about rate/2: we want him to break up the range of 0->(len(scalar)-1) at interval of rate. To make sure he makes it
	# to (len(scalar)-1) and does not generate intervals up to him we a rate/2 which should prevent arange from going beyond
	scalar_interp=numpy.arange(0, len(scalar)-1+rate/2, rate)
	while len(scalar_interp)<sample_count:
		scalar_interp=numpy.append(scalar_interp, scalar_interp)
	scalar_interp=scalar_interp[0:sample_count]
	scalar=numpy.interp(scalar_interp, scalar_indexes, scalar)
	return numpy.array(samples*scalar, dtype=samples.dtype)
