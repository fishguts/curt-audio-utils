"""
created: 5/22/15
@author: curt

Functions which abstracts encoding and deliver a consistent api for loading audio files.
All we know right now is 'wave' nonetheless it leaves room
"""
import numpy
from scipy.io import wavfile


def read(path):
	"""
	reads WAV encoding and returns tuple(sample_rate, samples)
	@param path: of file to load
	@return: tuple(sample_rate, samples)
	@raise: Exception if attempt to load fails
	"""
	try:
		srate, samples=wavfile.read(path)
		return srate, samples
	except Exception as ex:
		raise Exception("unable to parse '%s': %s"%(path, ex))


def write(path, srate, samples):
	"""
	writes file in WAV format
	@param path: to write file to
	@param srate: sample rate
	@param samples: 1D or (2, sample.count) or (sample.count, 2) numpy.array of samples
	@return:
	@raise: Exception if attempt to write fails
	"""
	try:
		# for 2 channel files scipy.io.wavfile expects the channel's samples to be arranged
		# side by side: left|right|left|right -> (samples, 2) and not (2, samples) which is easier to assemble
		if samples.ndim==2 and samples.shape[0]==2:
			# the way we achieve this using numpy is to flip the channels (right on top) and then
			# rotate the array down by 90 degrees so that the samples are arranged: left,right|left,right...
			samples=numpy.flipud(samples)
			samples=numpy.rot90(samples, k=-1)
		wavfile.write(path, srate, samples)
	except Exception as ex:
		raise Exception("unable to write '%s': %s"%(path, ex))

