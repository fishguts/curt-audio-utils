"""
created: 7/13/16
@author: curt
"""
import numpy

import fishguts
from fishguts import log
from fishguts.audio import afile


class AudioBufferMix(object):
	"""
	An interface through which you may specify the modes up yonder and then add buffers
	and have it do what the mode tells him to do.  The sample rate should be consistent
	otherwise you will get perceived pitch alterations
	"""
	MODE_MONO="mono"
	MODE_STEREO="stereo"
	MODE_NONE="none"
	_MODES=[MODE_MONO, MODE_STEREO, MODE_NONE]

	# --- public api ---#
	def __init__(self, srate, mode=MODE_MONO):
		log.write_warn_if_false(mode in self._MODES, "unknown mode mix '%s'", mode)
		super(AudioBufferMix, self).__init__()
		self.srate=srate
		self.mode=mode
		self.mixdown_buffer=None

	def mix_buffer(self, samples, srate, balance=0.5, path=None):
		log.write_warn_if_false(srate!=self.srate, "mixing srate={0} into buffer of srate={1}", srate, self.srate)
		if self.mode==self.MODE_MONO:
			self._mix_mono(samples)
		elif self.mode==self.MODE_STEREO:
			self._mix_stereo(samples=samples, balance=balance)
		else:
			afile.write(path=path, srate=self.srate, samples=samples)

	def write_buffer(self, path):
		if self.mixdown_buffer is not None:
			afile.write(path=path, srate=self.srate, samples=self.mixdown_buffer)

	# --- private api ---#
	def _mix_mono(self, samples):
		if self.mixdown_buffer is None:
			self.mixdown_buffer=numpy.copy(samples)
		else:
			if samples.size>self.mixdown_buffer.size:
				self.mixdown_buffer.resize(samples.size)
			self.mixdown_buffer[0:samples.size]+=samples

	def _mix_stereo(self, samples, balance):
		if self.mixdown_buffer is None:
			self.mixdown_buffer=numpy.zeros((2, samples.size), samples.dtype)
		elif samples.size>self.mixdown_buffer.shape[1]:
			self.mixdown_buffer.resize((2, samples.size), refcheck=False)
		# 0-count/2 => biased to left
		self.mixdown_buffer[0][0:samples.size]+=(samples*(1-balance)).astype(self.mixdown_buffer.dtype)
		# count/2-count => biased to right
		self.mixdown_buffer[1][0:samples.size]+=(samples*balance).astype(self.mixdown_buffer.dtype)
