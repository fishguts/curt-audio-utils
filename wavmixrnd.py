#!/usr/bin/env python

"""
Within a gamut specified with options - algorithmically picks source files and mixes them.
created: 5/23/15
@author: curt
"""
import subprocess
import os
import random
import math
import tempfile
import traceback

import numpy
import pydash

import fishguts
from fishguts import cmdline
from fishguts import log
from fishguts.audio import afile


class Commander:
	def __init__(self):
		self.options=None
		self.count=1
		self.repeat=False
		self.join=False
		self.patterns=[]
		self.input_paths=None
		self.input_files=[]
		self.output_prefix=None
		self.output_fname=None
		self.intervals=[]
		self.join_buffer=None
		self.join_srate=None

	#--- public api ---#
	def setup(self):
		"""
		Parse the command line and setup environment variables
		@raise Exception: if validation fails
		@return: True if successful and False if failed
		"""
		self.options, position=cmdline.parse(2, ["v", "vv", "r", "j"], ["a", "d", "c", "p", "d1", "d2", "i1", "i2", "I1", "I2", "qa", "qr", "qe", "qef"])
		# process options and positions
		log.set_verbose("v" in self.options, "vv" in self.options)
		self.count=int(fishguts.get(self.options, "c", self.count))
		self.repeat="r" in self.options
		self.join="j" in self.options
		self.input_paths=map(lambda p:os.path.abspath(p), position[0:-1])
		self.output_prefix=os.path.abspath(position[-1])
		# note: we will entirely pass off -q[x] processing to wavmix
		if not pydash.any_(["qa", "qr", "qe", "qef"],  lambda option: option in self.options):
			for index in range(2):
				for o in ["d", "i", "I"]:
					option="%s%d"%(o, index+1)
					if option in self.options:
						self.intervals.append({"option":option, "list":map(float, self._eval(self.options[option]))})
						fishguts.raise_if_false(len(self.intervals[-1]["list"])>0, "must specify interval list")
						break
			fishguts.raise_if_false(len(self.intervals)==2, "must specify two sets of intervals (-d, -i or -I)")
		if "qe" in self.options or "qef" in self.options:
			fishguts.raise_if_true("p" in self.options, "-qe[x] - pattern should be built into sequence")
		elif "p" not in self.options:
			self.patterns.extend(["f", "b", "fb", "bf"])
		else:
			for pattern in self.options["p"].split(","):
				self.patterns.append(pattern.strip().lower())
				if len(self.patterns[-1])==0 or len(self.patterns[-1].strip("fb"))>0:
					raise Exception("invalid pattern string '{0}'. supports combinations of 'f' and 'b'".format(self.patterns[-1]))


	@staticmethod
	def usage():
		print "usage: wavmixrnd.py [-v[v]crj] [-p <('f'|'b')+[,]  [-a attack_millis] [-d decay_millis]"
		print "        [-d[i] total_divisions]"
		print "        [-i[i] sample_interval] [-I[i] second_interval]"
		print "        [-qa quarter_millis:ratio1, ratio2, ratio1...]"
		print "        [-qr quarter_millis:<ratio1 & ratio2>, <ratio1 & ratio2>...]"
		print "        [-qe quarter_millis:(ratio, <0|1> [,advance|'b'|'f'] [,'b'|'f']), ...]"
		print "        [-qef <path>]"
		print "        <input1.wav> <input2.wav> [output.wav]"
		print "  -v: verbose"
		print "  -vv: very verbose"
		print "  -a:  attack in millis"
		print "  -d:  decay in millis"
		print "  -c: number of results to generate"
		print "  -r: allow inputs to be reused"
		print "  -j: use 'join' algorithm and append all results into a single output wav file"
		print "  -p: comma separated list of directions: <('f'|'b')+(,('f'|'b')+)*>"
		print "  -d[1|2]: comma separated list of divisions to choose from (~crossings=divisions+1). mutually exclusive with -i, -I"
		print "  -i[1|2]: comma separated list of min intervals in samples between crossings. mutually exclusive with -d, -I"
		print "  -I[1|2]: comma separated list of min intervals in millis between crossings. mutually exclusive with -d, -i"
		print "  -qa: alternating quantized rhythm: [quarter_in_millis:ratio_input[0], ratio_input[1], ratio_input[0], ...]"
		print "  	  ratio=multiple of quarter: 2=half, 1=quarter, .5=eight, .25=sixteenth"
		print "  -qr: repeated quantized rhythm: [quarter_in_millis:<ratio_input[0] & ratio_input[1]>, <ratio_input[0] & ratio_input[1]>, ...]"
		print "  	  ratio=multiple of quarter: 2=half, 1=quarter, .5=eight, .25=sixteenth"
		print "  -qe: sequenced arrangement: quarter_millis:(ratio, <0|1> [,advance|'b'|'f'] [,'b'|'f']), ...]"
		print "  	  ratio=multiple of quarter: 2=half, 1=quarter, .5=eight, .25=sixteenth"
		print "  	  <0|1>=the stream index"
		print "  	  [advance]=multiple of quarter by which to advance the stream after element is processed"
		print "  	  [b|f]=direction of sample block"
		print "  -qef: path based configuration. must have 'quarter_millis' value and 'get_sequence' function"


	def execute(self):
		"""
		Sequences operations and then mixes them according to env configuration
		"""
		config=None
		self._preprocess()
		for index in range(self.count):
			config=self._select_mix_config(index, config)
			self._generate_mix(**config)
			self._postprocess_mix(config)
		self._postprocess()


	#--- private api ---#
	@staticmethod
	def _eval(expression):
		"""
		Evals an expression using ole 'eval' but adds to the global context some handy aliases to numpy methods
		@param expression:
		@return:
		"""
		return eval(expression, {
			"frange": numpy.arange,
			"linspace": numpy.linspace,
			"sin": numpy.sin,
			"log": numpy.log10,
			"sqrt": numpy.sqrt
		})

	def _preprocess(self):
		self._build_input_list()
		if self.join:
			self.output_fname=lambda index:tempfile.mktemp()
			if not self.output_prefix.lower().endswith(".wav"):
				self.output_prefix+=".wav"
		else:
			template="%s%%0%dd.wav"%(os.path.abspath(self.output_prefix), math.log10(self.count)+1)
			self.output_fname=lambda index:template%(index)


	def _postprocess(self):
		if self.join_buffer is not None:
			log.write_verbose("writing compilation to '{0}'", os.path.relpath(self.output_prefix))
			# I think we should assume that the prefix is a fully contructed path and filename
			afile.write(path=self.output_prefix, srate=self.join_srate, samples=self.join_buffer)
			self.join_buffer=None


	def _build_input_list(self):
		"""
		Builds list of eligible audio files found in all of our self.input_paths
		@raise Exception: if no input audio files are found or too few are found
		"""
		log.write_verbose("finding eligible input audio files")
		for path in self.input_paths:
			files=os.listdir(path)
			for f in files:
				if f.lower().endswith(".wav"):
					self.input_files.append(os.path.join(path, f))
		fishguts.raise_if_false(len(self.input_files)>0, "no input audio files found")
		if "r" not in self.options and len(self.input_files)<self.count*2:
			raise Exception("too few audio files found ({0}) to generate {0} combinations. consider '-r' option".format(len(self.input_files), self.count))
		log.write_verbose(" - found {0} audio files", len(self.input_files))


	def _select_mix_config(self, index, prev=None):
		"""
		Selects a file pair of inputs, intervals a pattern and generates output file name
		"""
		log.write_verbose("generating mix configuration #{0}", index+1)
		if prev is None or not self.join:
			inputs=random.sample(self.input_files, 2)
			if not self.repeat:
				pydash.each(inputs, self.input_files.remove)
		elif index%2==0:
			inputs=(prev["input1"], random.sample(self.input_files, 1)[0])
			if not self.repeat:
				self.input_files.remove(inputs[1])
		else:
			inputs=(random.sample(self.input_files, 1)[0], prev["input2"])
			if not self.repeat:
				self.input_files.remove(inputs[0])
		result={
			"input1":inputs[0],
			"input2":inputs[1],
			"output":self.output_fname(index+1),
			"options":[]
		}
		for option in ("v", "vv"):
			if option in self.options:
				result["options"].extend(("-"+option,))
		for option in ("a", "d"):
			if option in self.options:
				result["options"].extend(("-"+option, self.options[option]))
		if "p" in self.options:
			result["options"].extend(("-p", random.choice(self.patterns)))
		if "qa" in self.options:
			result["options"].extend(("-qa", self.options["qa"]))
		elif "qr" in self.options:
			result["options"].extend(("-qr", self.options["qr"]))
		elif "qe" in self.options:
			result["options"].extend(("-qe", self.options["qe"]))
		elif "qef" in self.options:
			result["options"].extend(("-qef", self.options["qef"]))
		else:
			result["options"].extend((
				"-"+self.intervals[0]["option"], random.choice(self.intervals[0]["list"]),
				"-"+self.intervals[1]["option"], random.choice(self.intervals[1]["list"])
			))
		return result


	def _generate_mix(self, options, input1, input2, output):
		"""
		Calls wavmix.py to do what needs to be done
		"""
		args=pydash.flatten([
			("wavmix.py",),
			map(lambda o: str(o), options),
			(input1, input2, output)
		])
		log.write_verbose(" ".join(args))
		subprocess.call(args=args)


	def _postprocess_mix(self, config):
		if self.join:
			# hold on the last sample rate for writing. if they vary then we will have some funny results but is beyond scope of this tool
			self.join_srate, samples=afile.read(config["output"])
			if self.join_buffer is None:
				self.join_buffer=samples
			else:
				self.join_buffer.resize((self.join_buffer.size+samples.size,), refcheck=False)
				self.join_buffer[-samples.size:]=samples


#--- Action ---#
captain=Commander()
try:
	captain.setup()
except Exception as exception:
	log.write_error(exception)
	captain.usage()
	exit(1)

try:
	captain.execute()
except Exception as exception:
	log.write_failed(exception)
	traceback.print_exc()
	exit(1)
