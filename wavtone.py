#!/usr/bin/env python

"""
Generates a waveform and writes to file/stdout
created: 5/25/15
@author: curt
"""

import os

from fishguts import cmdline
from fishguts import log
from fishguts.audio import afile
from fishguts.audio import asignal


# Globals
options=None
fout=None
wform="sin"
freq=220
amp=0x7fff/4*3
slen=1.0
srate=22050


# Functional support
def setup():
	"""
	Parse the command line and setup environment variables
	@return: True if successful and False if failed
	"""
	try:
		global options, fout, wform, freq, amp, slen, srate
		options, position=cmdline.parse(0, ["v"], ["w", "f", "a", "sr", "ds", "dt"])
		log.set_verbose("v" in options)
		fout=os.path.abspath(position[0]) if len(position)>0 else os.path.abspath("a.wav")
		# waveform
		if "w" in options:
			wform=options["w"]
		# frequency
		if "f" in options:
			freq=float(options["f"])
		# amplitude
		if "a" in options:
			amp=int(options["a"])
		# sample rate
		if "sr" in options:
			srate=int(options["sr"])
		# duration
		if "ds" in options:
			slen=int(options["ds"])
		else:
			slen=float(options["dt"]) if "dt" in options else slen
			slen=int(slen*srate)
		# validation
		if wform not in ["sin"]:
			raise Exception("unknown waveform '%s'"%(wform,))
	except Exception as exception:
		log.write_error(exception)
		print "usage: wavtone.py [-v] [-sr sample_rate:22050] [-w waveform:sin] [-f frequency:220] [-a amplitude:~24575] [-ds duration_samples] [-dt duration_seconds] [output.wav]"
		print "  -v: verbose"
		print "  -sr: sample rate"
		print "  -w: waveform: sin"
		print "  -a: amplitude in sample units"
		print "  -dt: duration in time|seconds"
		print "  -ds: duration in samples"
		return False
	return True


def execute():
	"""
	Generates the waveform and writes to output.wav
	"""
	samples=None
	if wform=="sin":
		log.write_verbose("creating sine wave: freq={0:.2f}, slen={1}, amp={2}", freq, slen, amp)
		samples=asignal.create_sin_samples(srate=srate, freq=freq, slen=slen, amp=amp)
	else:
		assert()
	log.write_verbose("writing {0}", os.path.relpath(fout))
	afile.write(path=fout, srate=srate, samples=samples)


#--- Action ---#
if not setup():
	exit(1)
else:
	try:
		execute()
	except Exception as exception:
		log.write_failed(exception)


