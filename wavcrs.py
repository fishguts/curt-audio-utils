#!/usr/bin/env python

"""
Finds zero crossings and dumps them to file or stdout
created: 5/20/15
@author: curt
"""

import os
import struct
import sys

from fishguts import cmdline
from fishguts import log
from fishguts.audio import afile
from fishguts.audio import ainfo


# Globals
options=None
binary=False
interval=0	# (samples, time)
file_path_source=None
file_path_target=None
file_out=None


# Functional support
def setup():
	"""
	Parse the command line and setup environment variables
	@return: True if successful and False if failed
	"""
	try:
		global options, binary, interval
		global file_path_source, file_path_target, file_out
		options, position=cmdline.parse(1, ["v", "f", "b"], ["i", "I", "d"])
		file_path_source=os.path.abspath(position[0])
		log.set_verbose("v" in options)
		binary=True if "b" in options else False
		if "d" in options:
			interval=int(options["d"])
		elif "i" in options:
			interval=int(options["i"])
		elif "I" in options:
			interval=float(options["I"])
		else:
			interval=1
		if True if "f" in options else False:
			spath, sfile=os.path.split(file_path_source)
			if binary:
				file_out=file(os.path.join(spath, "%s-zero.bin"%(os.path.splitext(sfile)[0])), "wb")
			else:
				file_out=file(os.path.join(spath, "%s-zero.txt"%(os.path.splitext(sfile)[0])), "w")
		else:
			file_out=sys.stdout
	except Exception as exception:
		log.write_error(exception)
		print "usage: wavcrs.py [-vfb] [-d total_divisions] [-i sample_interval] [-I second_interval] <input.wav>"
		print "  -v: verbose"
		print "  -f: write to file as opposed to stdout"
		print "  -b: format as binary"
		print "  -d: desired number of divisions (~crossings=divisions+1). mutually exclusive with: -i, -I"
		print "  -i: min interval in samples between crossings. mutually exclusive with: -d, -I"
		print "  -I: min interval in seconds between crossings. mutually exclusive with: -d, -i"
		return False
	return True


def execute():
	"""
	Loads file, finds zero crossings and dumps
	"""
	global interval
	srate, samples=afile.read(file_path_source)
	# now that we have the sample rate - compute the min_interval parts: (samples, time)
	if "d" in options:
		sample_interval=len(samples)/max(1, interval)
		interval=(sample_interval, sample_interval/float(srate))
	elif "I" in options:
		interval=(max(1, int(srate*interval)), interval)
	else:
		interval=(max(1, interval), interval/float(srate))
	log.write_verbose("read {0}: sample_rate={1}, sample_count={2}, interval=({3}|{4:f}s)",
		os.path.relpath(file_path_source), srate, len(samples), interval[0], interval[1])
	# perform our function
	crossings=ainfo.get_zero_crossings(samples, interval[0])
	write_crossings(crossings)


def write_crossings(crossings):
	log.write_verbose("writing {0}: crossing_count={1}", os.path.relpath(file_out.name), len(crossings))
	if not binary:
		file_out.write(",".join([str(crossing) for crossing in crossings]))
	else:
		# little endian 32 bit encoding
		for crossing in crossings:
			file_out.write(struct.pack("<i", crossing))
	file_out.flush()


#--- Action ---#
if not setup():
	exit(1)
else:
	try:
		execute()
		file_out.close()
	except Exception as exception:
		log.write_failed(exception)


