#!/usr/bin/env python

"""
Uses MIDI file as a set of rules for mixing one or more audio sources
created: 7/4/2016
@author: curt
"""

from __future__ import division

import bisect
import os
import pprint
import random
import sys
import traceback

import midi
import numpy
import pydash

import fishguts
from fishguts import audio
from fishguts import cmdline
from fishguts import log
from fishguts.audio import afile
from fishguts.audio import ainfo
from fishguts.audio.amix import AudioBufferMix


class Commander:
	"""
	Mixes two source waves and writes results to designated file or a.wav
	"""

	def __init__(self):
		self.options=None
		self.config=None
		self.instruments=[]
		self.mix_buffer=None
		self.globals={
			"pi": numpy.pi,
			"sin": numpy.sin,
			"log": numpy.log10,
			"sqrt": numpy.sqrt,
			"randomi": random.randint,
			"randomf": random.uniform
		}

	# --- public interface---#
	@staticmethod
	def usage():
		print "usage: midimix.py [-v,-vv] [conf.json]"
		print "  -v: verbose"
		print "  -vv: very verbose"

	def setup(self):
		"""
		Reads the command line and setup environment variables
		@raise: if parsing fails
		"""
		self.options, position=cmdline.parse(0, ["v", "vv"])
		log.set_verbose("v" in self.options, "vv" in self.options)
		log.write_verbose("processing command line")
		self.config=fishguts.path_to_json(fishguts.get(position, "0", sys.stdin))
		self.config["mix"]=fishguts.get(self.config, "mix", "none")
		if self.config["mix"]!="none":
			self.config["target"]=self.config.get("target", "a.wav")
		pydash.merge(self.globals, self.config.get("globals", {}))
		# do some basic validation
		fishguts.raise_if_none(self.config.get("source", None), "missing source")
		fishguts.raise_if_none(pydash.is_iterable(self.config.get("tracks", None)), "missing or poorly configured tracks")
		for track in self.config["tracks"]:
			fishguts.raise_if_false("source" in track, "poorly configured tracks: missing 'source'")
			if self.config["mix"]=="none":
				fishguts.raise_if_false("target" in track, "poorly configured tracks: missing 'target'")

	def execute(self):
		"""
		Sequences operations and then mixes them according to env configuration
		"""
		self._load_files()
		self._config_instrument()
		self._build_sequences()
		self._write_results()

	# --- private api ---#
	def _eval(self, code, _compile=False):
		"""
		Evals or compiles and evals an expression using ole 'eval' but adds to the global context.
		@param code:
		@return:
		"""
		try:
			if _compile:
				code=compile(code, "<string>", "exec")
			return eval(code, self.globals)
		except Exception as exception:
			raise Exception("attempt to eval '{0}' failed - {1}".format(code, exception))

	def _load_files(self):
		"""
		Loads midi file and all dependent audio files
		@return: None
		"""
		log.write_verbose("loading {0}:", os.path.relpath(self.config["source"]))
		self.config["_source"]=midi.read_midifile(self.config["source"])
		self.config["_converter"]=self.config["_source"].get_tick_converter()
		for track_index in range(len(self.config["tracks"])):
			track_cfg=self.config["tracks"][track_index]
			# 1. find the midi track
			if "track.name" in track_cfg:
				track_cfg["_midi"]=pydash.find(self.config["_source"], lambda track: track.get_text(midi.TrackNameEvent.metacommand)==track_cfg["track.name"])
				fishguts.raise_if_none(track_cfg["_midi"], "could not find 'track.name'={0}".format(track_cfg["track.name"]))
			elif "track.instrument" in track_cfg:
				track_cfg["_midi"]=pydash.find(self.config["_source"],
					lambda track: track.get_text(midi.InstrumentNameEvent.metacommand)==track_cfg["track.instrument"])
				fishguts.raise_if_none(track_cfg["_midi"], "could not find 'track.instrument'={0}".format(track_cfg["track.instrument"]))
			else:
				track_cfg["_midi"]=self.config["_source"][track_cfg.get("track.index", track_index)]
				fishguts.raise_if_none(track_cfg["_midi"], "could not find 'track.index'={0}".format(fishguts.get(track_cfg, "track.index", track_index)))
			track_cfg["_midi.name"]=track_cfg["_midi"].get_text(metacommand=midi.TrackNameEvent.metacommand, dfault="[unknown]")
			# 2. load the audio
			log.write_verbose("loading {0}:", os.path.relpath(track_cfg["source"]))
			source=afile.read(track_cfg["source"])
			track_cfg["_source"]={
				"srate": source[0],
				"samples": source[1],
				"crossings": ainfo.get_zero_crossings(samples=source[1], slope=False)
			}
			track_cfg["_target"]={
				"srate": source[0]
			}
			log.write_verbose(" - rate={0}, samples={1}, crossings={2}", track_cfg["_source"]["srate"], len(track_cfg["_source"]["samples"]),
				len(track_cfg["_source"]["crossings"]))

	def _config_instrument(self):
		instruments=fishguts.get(self.config, "instrument", tuple())
		if not isinstance(instruments, list):
			instruments=(instruments,)
		for instrument in instruments:
			self.instruments.append({
				"waveform": fishguts.get(instrument, "waveform", "sin"),
				"attack": fishguts.get(instrument, "attack", 0),
				"decay": fishguts.get(instrument, "decay", 0),
				"amplitude": fishguts.get(instrument, "amplitude", 1)
			})

	def _build_sequences(self):
		log.write_verbose("generating sequences:")
		ctracks=len(self.config["tracks"])
		for itrack in range(ctracks):
			track=self.config["tracks"][itrack]
			srate=track["_target"]["srate"]
			log.write_verbose(" - generating track #{0}: {1}", itrack, track["_midi.name"])
			samples=self._build_sequence(track, self.instruments[itrack%len(self.instruments)])
			if self.mix_buffer is None:
				self.mix_buffer=AudioBufferMix(srate=srate, mode=self.config["mix"])
			if self.config["mix"]==AudioBufferMix.MODE_NONE:
				log.write_verbose(" - writing {0}", os.path.relpath(track["target"]))
				self.mix_buffer.mix_buffer(samples=samples, srate=srate, path=track["target"])
			else:
				self.mix_buffer.mix_buffer(samples=samples, srate=srate, balance=float(itrack)/(ctracks-1) if ctracks>1 else 0.5)

	def _build_sequence(self, track, instrument):
		toffset_source=0		# running midi-tick offset
		send_target=0			# sample offset of the end
		srate=track["_source"]["srate"]
		crossings=track["_source"]["crossings"]
		converter=self.config["_converter"]
		advance_exp=fishguts.get(self.config, "midi.advance", "note.duration")
		volume_exp=fishguts.get(self.config, "midi.volume", "1")
		samples_source=track["_source"]["samples"]
		samples_target=numpy.zeros(srate, dtype=numpy.int16)
		self.globals["track"]=track
		for event in track["_midi"]:
			if isinstance(event, midi.NoteOnEvent):
				self.globals["note"]=event
				volume=self._eval(volume_exp)
				# 1. calculate our source offset and target offset
				if toffset_source>=0:
					soffset_source=audio.seconds_to_samples(srate, converter.offset_to_seconds(toffset_source))
					soffset_source=crossings[bisect.bisect_left(crossings, soffset_source%len(samples_source), hi=len(crossings)-1)]
				else:
					# We have backed up beyond the beginning of the source. If we modulus'ed the tick relative to the song or track end then it will
					# plop us into some seemingly random spot in the source track which will be confusing.  I think the assumption would be that they
					# want to wrap around to the end.  So, we use a positive toffset_source to find the would be time offset in our midi track
					# and then subtract it from the end of our source audio track.
					soffset_source=audio.seconds_to_samples(srate, converter.offset_to_seconds(0-toffset_source))
					soffset_source=crossings[bisect.bisect_left(crossings, len(samples_source)-soffset_source%len(samples_source), hi=len(crossings)-1)]
				soffset_target, scount_target=[audio.seconds_to_samples(srate, seconds) for seconds in converter.event_to_seconds(event)]
				# 2. clone
				samples=audio.copy_samples(samples=samples_source, ioffset=soffset_source, count=scount_target, algorithm=track.get("copy", None))
				scount_target=min(scount_target, len(samples))
				# 3. effect
				if volume!=1.0:
					samples=(samples*volume).astype(samples.dtype)
				if instrument["attack"]>0:
					sattack=min(instrument["attack"], scount_target)
					audio.scale_samples(samples, numpy.arange(0, sattack)/sattack)
				if instrument["decay"]>0:
					sdecay=min(instrument["decay"], scount_target)
					audio.scale_samples(samples, numpy.arange(sdecay, 0, -1)/sdecay, scount_target-sdecay)
				# 4. mix the samples
				if soffset_target+scount_target>=len(samples_target):
					samples_target.resize(max(len(samples_target)*2, soffset_target+scount_target), refcheck=False)
				if track.get("overlap", "mix")=="mix":
					samples_target[soffset_target:soffset_target+scount_target]+=samples
				else:
					samples_target[soffset_target:soffset_target+scount_target]=samples
				# 5. advance the our trackers
				toffset_source+=int(self._eval(advance_exp))
				send_target=max(send_target, soffset_target+scount_target)
				print "soffset={0}, note={1}".format(toffset_source, pprint.pformat(event))
				del self.globals["note"]
		# cleanup and get out of here
		del self.globals["track"]
		return samples_target[:send_target]

	def _write_results(self):
		if self.config["mix"]!=AudioBufferMix.MODE_NONE:
			log.write_verbose("writing {0}", os.path.relpath(self.config["target"]))
			self.mix_buffer.write_buffer(path=self.config["target"])


# --- Action ---#
captain=Commander()
try:
	captain.setup()
except Exception as exception:
	log.write_error(exception)
	captain.usage()
	exit(1)

try:
	captain.execute()
except Exception as exception:
	log.write_failed(exception)
	# traceback.print_exc()
	exit(1)
