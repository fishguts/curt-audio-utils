#!/usr/bin/env python

"""
created: 12/4/15
@author: curt
"""

import traceback
from datetime import datetime

import numpy
import pydash
import sys
from scipy import ndimage

import fishguts
from fishguts import cmdline
from fishguts import log

class Commander:
	"""
	Generate a normalized sequence from an image
	I am the captain. I am the commander.
	"""

	def __init__(self, ):
		self.options=None
		self.image_path=None
		self.image_data=None
		self.output_path=None

	# --- public api ---#
	def setup(self):
		"""
		Parse the command line and setup environment variables
		@raise Exception: if validation fails
		@return: True if successful and False if failed
		"""
		self.options, position=cmdline.parse(1, ["v", "vv", "dd"], ["cx", "cy"])
		log.set_verbose("v" in self.options, "vv" in self.options)
		log.write_verbose("parsing command line")
		self.image_path=position[0]
		self.file_out=fishguts.get(position, "1", sys.stdout)
		for option in ("cx", "cy"):
			if option not in self.options:
				self.options[option]=(1, 1, 0)
			else:
				self.options[option]=eval(self.options[option])
				if not pydash.is_iterable(self.options[option]):
					self.options[option]=(self.options[option], self.options[option], 0)
				else:
					fishguts.raise_if_false(pydash.is_iterable(self.options[option]), "-{0} not iterable".format(option))
					fishguts.raise_if_false(1<=len(self.options[option])<=3, "-{0} incorrect length".format(option))
					if len(self.options[option])>1:
						fishguts.raise_if_false(self.options[option][0]>=self.options[option][1], "-{0} scan may not be greater than stride".format(option))
				fishguts.raise_if_false(pydash.all_(self.options[option], pydash.is_integer), "-{0} iterables must be integers".format(option))

	def usage(self):
		print "usage: imgnrml.py [-v[v]] [-dd]"
		print "        [-cx (scan, stride=scan, offset=0)]"
		print "        [-cy (scan, stride=scan, offset=0)]"
		print "        <image> [output.json=stdout]"
		print "  -v: verbose"
		print "  -vv: very verbose"
		print "  -dd: don't distribute over channel max and mins found in file. use max=255 and min=0"

	def execute(self):
		self._load_image()
		streams=self._process_image()
		self._write_data(streams)

	# --- private api ---#
	def _load_image(self):
		log.write_verbose("loading {0}".format(self.image_path))
		self.image_data=ndimage.imread(self.image_path)

	def _process_image(self):
		"""
		iterates through the (already loaded) image and applies the "cx" and "cy" algorithms to the image
		and reduces blocks of one or more pixels down to a single float by averaging all components in that block
		@return:
		"""
		scanx=self.options["cx"][0]
		scany=self.options["cy"][0]
		stridex=fishguts.get(self.options["cx"], 1, scanx)
		stridey=fishguts.get(self.options["cy"], 1, scany)
		offsetx=fishguts.get(self.options["cx"], 2, 0)
		offsety=fishguts.get(self.options["cy"], 2, 0)
		sourceshape=self.image_data.shape
		targetshape=((sourceshape[0]-offsety)/stridey, (sourceshape[1]-offsetx)/stridex)
		result=numpy.zeros(targetshape, dtype=numpy.float32)
		for iy in xrange(offsety, sourceshape[0]-stridey+1, stridey):
			for ix in xrange(offsetx, sourceshape[1]-stridex+1, stridex):
				sample=self.image_data[iy:iy+scany, ix:ix+scanx]
				mean=numpy.mean(sample)
				result[(iy-offsety)/stridey, (ix-offsetx)/stridex]=mean
		if "dd" in self.options:
			result/=255.0
		else:
			result-=result.min()
			result/=result.max()
		return result

	def _write_data(self, streams):
		log.write_verbose("writing {0} streams to {1}".format(len(streams), self.file_out))
		data={
			"meta": {
				"algorithm": fishguts.scrub({
					"cx": self.options.get("cx"),
					"cy": self.options.get("cy")
				}),
				"source": self.image_path,
				"created": datetime.now().isoformat()
			},
			"data": [ {
				"name": str(i),
				"sequence": map(lambda v: round(v, 4), streams[i])
			} for i in xrange(len(streams))]
		}
		fishguts.json_to_path(self.file_out, data)


# --- Action ---#
captain=Commander()
try:
	captain.setup()
except Exception as exception:
	log.write_error(exception)
	captain.usage()
	exit(1)

try:
	captain.execute()
except Exception as exception:
	log.write_failed(exception)
	traceback.print_exc()
	exit(1)
