#!/usr/bin/env python

"""
Plays a call-n-answer sort of game between two tracks
created: 10/9/16
@author: curt
"""

from __future__ import division

import copy
import math
import os
import re
import traceback
from os import path

import midi
import pydash
import sys

import fishguts
from fishguts import cmdline
from fishguts import log
from fishguts import parse


class Commander:
	"""
	Uses midi file as an outline and interpolates notes according to our rules
	"""
	power_regex=r"^power(\d+|\d+\.\d+|\.\d+)?$"

	def __init__(self):
		self.dir_app=os.path.dirname(os.path.realpath(__file__))
		self.options=None
		self.config=None
		self.path_source=None
		self.path_target=None
		self.midi_data=None
		self.tracks=[]

	# --- public interface---#
	@staticmethod
	def usage():
		print "usage: miditalk.py [-v,-vv] [-c conf.json] <source.mid> [target.mid]"
		print "  -v: verbose"
		print "  -vv: very verbose"
		print "  -c: configuration file. Defaults to config/miditalk_conf.json"
		print "  -p.f: feedback pattern playback: false"
		print "  -p.r: number of times to repeat a pattern: 1"
		print "  -p.R: reverse repeated patterns: false"
		print "  -p.x: mutual exclusion of echoes overlapping other patterns: false"
		print "  -v.c: velocity curve: linear|power\d?"
		print "  -v.d: decay quarters: 8"
		print "  -v.m: velocity minimum: 2"
		print "  -v.s: starting velocity ratio: 0.75"

	def setup(self):
		"""
		Reads the command line and setup environment variables
		@raise: if parsing fails
		"""
		curve_pattern=r"^linear$|{0}".format(self.power_regex)
		overrides=(
			# (option, path, optional validator)
			("p.f", "pattern.feedback", parse.to_bool, lambda v: True),
			("p.x", "pattern.mutex", parse.to_bool, lambda v: True),
			("p.r", "pattern.repeat", int, lambda v: v>0),
			("p.R", "pattern.reverse", parse.to_bool, lambda v: True),
			("v.c", "velocity.curve", None, lambda v: re.match(curve_pattern, v) is not None),
			("v.d", "velocity.decay", float, lambda v: v>0),
			("v.m", "velocity.min", int, lambda v: 128<v<0),
			("v.s", "velocity.start", float, lambda v: v>0)
		)
		self.options, position=cmdline.parse(1, ["v", "vv"], ["c"]+[override[0] for override in overrides])
		log.set_verbose("v" in self.options, "vv" in self.options)
		log.write_verbose("processing command line")
		self.path_source=position[0]
		if len(position)>1:
			self.path_target=position[1]
		else:
			self.path_target="{0}-miditalk{1}".format(*path.splitext(self.path_source))
		self.config=fishguts.path_to_json(os.path.join(self.dir_app, "./config/miditalk_conf.json"))
		if "c" in self.options:
			pydash.merge(self.config, fishguts.path_to_json(self.options["c"]))
		# apply option overrides directly to self.config
		for override in overrides:
			if override[0] in self.options:
				value=self.options[override[0]]
				if override[2] is not None:
					value=override[2](value)
				self.config=pydash.set_(self.config, override[1], value)
				# validation
				if override[3] is not None:
					fishguts.raise_if_false(override[3](value), "-{0}: invalid value '{1}'".format(override[0], value))


	def execute(self):
		"""
		Sequences operations and then mixes them according to env configuration
		"""
		self._load_files()
		self._build_islands()
		self._generate_target()
		self._write_results()

	# --- private api ---#
	def _load_files(self):
		"""
		Loads midi file and all dependent audio files
		@return: None
		"""
		log.write_verbose("loading {0}:", os.path.relpath(self.path_source))
		self.midi_data=midi.read_midifile(self.path_source)

	def _write_results(self):
		log.write_verbose("writing {0}", os.path.relpath(self.path_target))
		midi.write_midifile(self.path_target, self.midi_data)

	def _build_islands(self):
		for track in self.midi_data:
			archipelago=self._build_track_islands(track)
			# if this is the first track and is nothing but meta-data then we don't want to process him
			if len(archipelago)>0 or len(self.tracks)>0:
				self.tracks.append(archipelago)

	def _build_track_islands(self, track):
		islands=[]
		for event in track:
			if isinstance(event, midi.NoteEvent):
				islands.append(Island(
					commander=self,
					track=track,
					start=event.offset,
					stop=event.offset+event.duration,
					events=[event]
				))
		ii=1
		while ii<len(islands):
			stop_quantized=islands[ii-1].start+islands[ii-1].get_duration_q()
			if max(islands[ii-1].stop, stop_quantized)<islands[ii].start:
				ii+=1
			else:
				islands[ii-1].stop=max(islands[ii-1].stop, islands[ii].stop)
				islands[ii-1].events.extend(islands[ii].events)
				del islands[ii]
		return islands

	def _generate_target(self):
		for itrack in range(1, len(self.tracks), 2):
			self._generate_conversation(self.tracks[itrack-1], self.tracks[itrack])
			self._generate_conversation(self.tracks[itrack], self.tracks[itrack-1])

	def _generate_conversation(self, track_from, track_to):
		for iisland_to in range(len(track_to)):
			island_to=track_to[iisland_to]
			# echo algorithm: let's try end on or before the next guy starts
			island_from=pydash.find_last(track_from, lambda island: island.stop<=island_to.start)
			if island_from is not None:
				max_offset=sys.maxint
				if self.config["pattern"]["mutex"]:
					# todo: do we want to isolate this behavior to the same track or the from track as well?
					max_offset=getattr(pydash.get(track_to, iisland_to+1), "start", max_offset)
				velocity_curve=self._get_velocity_curve(island_to.start+island_to.get_duration_q())
				for event in self._generate_echo_sequence(island_from, island_to):
					if event.offset>=max_offset:
						break
					else:
						event.velocity=velocity_curve(event.velocity, event.offset)
						island_to.track.insert_event(event)


	def _generate_echo_sequence(self, island_from, island_to):
		def _copy_events(cf, ct, offset=0, clone=True):
			for event in cf:
				if clone:
					event=copy.deepcopy(event)
				event.offset+=offset
				ct.append(event)
		result=[]
		island_from_duration_q=island_from.get_duration_q()
		island_to_duration_q=island_to.get_duration_q()
		# 1. pack in the from events
		offset=island_to.start+island_to_duration_q
		for rindex in range(0, self.config["pattern"]["repeat"]):
			interval_offset=offset+island_from_duration_q*rindex
			for event in island_from.events:
				clone=copy.deepcopy(event)
				if self.config["pattern"]["reverse"]:
					clone.offset=interval_offset+island_from.duration-clone.duration-(clone.offset-island_from.start)
				else:
					clone.offset=interval_offset+(clone.offset-island_from.start)
				result.append(clone)
		# 2. pack in existing feedback
		_copy_events(island_from.feedback, result, offset+island_from_duration_q*self.config["pattern"]["repeat"], clone=False)
		# 3. build feedback. the way it works is that we "feedback" what was last played in entirety: island_to + island_from + prior
		if self.config["pattern"]["feedback"]:
			island_from.feedback=[]
			_copy_events(island_to.events, island_from.feedback, 0-island_to.start)
			_copy_events(result, island_from.feedback, 0-island_to.start)
		return result

	#--- todo: will we use this approach? ---#
	def _get_velocity_curve(self, offset):
		name=self.config["velocity"]["curve"]
		duration=self.midi_data.resolution*self.config["velocity"]["decay"]
		start_ratio=self.config["velocity"]["start"]
		vmin=self.config["velocity"]["min"]
		if name=="linear":
			return self._curve_linear(offset, duration, start_ratio, vmin)
		elif name.startswith("power"):
			return self._curve_power(offset, duration, start_ratio, vmin, float(re.match(self.power_regex, name).groups(2)[0]))
		else:
			log.write_failed("velocity curve '{0}'?", name)

	@staticmethod
	def _curve_linear(offset, duration, start_ratio, vmin):
		def calculate(velocity, roffset):
			ratio=1-(roffset-offset)/duration
			return max(vmin, int(ratio*velocity*start_ratio))
		return calculate

	@staticmethod
	def _curve_power(offset, duration, start_ratio, vmin, power=2):
		base=math.pow(duration, 1/power)
		def calculate(velocity, roffset):
			ratio=1-(roffset-offset)/duration
			return max(vmin, int(start_ratio*velocity*math.pow(base*ratio, power)/duration))
		return calculate


class Island(object):
	def __init__(self, commander, track, start, stop, events=None, feedback=None):
		self.commander=commander
		self.track=track
		self.start=start
		self.stop=stop
		self.events=[] if events is None else events
		self.feedback=[] if feedback is None else feedback

	def get_duration(self):
		return self.stop-self.start
	duration=property(get_duration)

	def get_duration_q(self):
		return self.commander.midi_data.nearest_quantized_duration(self.stop-self.start)


# --- Action ---#
captain=Commander()
try:
	captain.setup()
except Exception as exception:
	log.write_error(exception)
	captain.usage()
	exit(1)

try:
	captain.execute()
except Exception as exception:
	log.write_failed(exception)
	traceback.print_exc()
	exit(1)
