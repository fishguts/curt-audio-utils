"""
created: 11/18/15
@author: curt
"""

from __future__ import division

import fishguts
import numpy


#--- reference library ---#
def progressive_cycle_linear(base_millis=117.8, reverse=True):
	"""
	The cycle gets progressively shorter and shorter and subsequently things get faster and faster
	@param base_millis:
	@param reverse:
	@return:
	"""
	sequences=[]
	period_count=75				# how many periods is a whole cycle broken up into
	sequence_divisions=5		# how many sequences per idata per single period
	for scalar in numpy.linspace(4, 0.5, period_count):
		sequence=[]
		ioffset=reversed(numpy.linspace(0, 1, sequence_divisions)) if reverse else iter(numpy.linspace(0, 1, sequence_divisions))
		for _ in range(sequence_divisions):
			sequence.append({
				"idata": 0,
				"offset": ioffset.next(),
				"direction": "f",
				"ratio": 1.0/(sequence_divisions*1.75)
			})
			sequence.append({
				"idata": 1,
				"ratio": 1.0/(sequence_divisions*2)
			})
		sequence.append({
			"advance": 0.25,
			"ratio": 0
		})
		sequences.append({
			"quarter_millis": base_millis*scalar,
			"sequence": sequence
		})
	sequences.append({
		"quarter_millis": base_millis,
		"sequence": [{
			"advance": 2,
			"ratio": 2
		}]
	})
	return sequences

def progressive_start_sin():
	"""
	increases the ratio as per sin function and forwards offset at a separate rate
	@return:
	"""
	sequence=[]
	divisions=18
	for direction in "fb":
		advanced=0
		for ratio in fishguts.isin(divisions, 1.0/divisions, 0.5, count=divisions/2, phase=numpy.pi*3/2):
			sequence.append({
				"idata": 0,
				"ratio": ratio,
				"attack": ratio/4.25,
				"decay": ratio/8,
				"direction": direction
			})
			sequence.append({
				"idata": 0,
				"ratio": 0,
				"advance": 1.0/(divisions*2)
			})
			advanced+=1.0/(divisions*2)
		sequence.append({
			"idata": 0,
			"ratio": 0,
			"advance": 1.0-advanced
		})
	return {
		"sequence": sequence,
		"quarter_millis": 2599.74/2
	}


def fast_staccato_reversed(quarter_millis=672, section_length=1, quarter_divisions=5, advance_rate=1/4, algorithm=reversed):
	"""
	reversed sequence
	@param quarter_divisions:
	@return:
	"""
	sequence=[]
	ioffset=algorithm(numpy.linspace(0, section_length, section_length*quarter_divisions))
	for i in range(section_length*quarter_divisions):
		sequence.append({
			"idata": 0,
			"offset": ioffset.next(),
			"ratio": 1.0/(quarter_divisions*1.75)
		})
		sequence.append({
			"idata": 1,
			"ratio": 1.0/(quarter_divisions*2)
		})
	sequence.append({
		"idata": 0,
		"advance": advance_rate*section_length,
		"ratio": 0
	})
	sequence.append({
		"idata": 1,
		"advance": advance_rate*section_length,
		"ratio": 0
	})
	return {
		"quarter_millis": quarter_millis,
		"sequence": sequence
	}


def fast_staccato_randomized(quarter_millis=526.4/4, section_length=4, quarter_divisions=4, advance_rate=1/2):
	sequence=[]
	for _ in range(4):
		series=fishguts.irandom(numpy.arange(0, section_length, 1/quarter_divisions))
		for index in range(16):
			sequence.append({
				"idata": 0,
				"offset": series.next(),
				"ratio": 0.5
			})
			sequence.append({
				"idata": 1,
				"ratio": 0.25
			})
		sequence.append({
			"idata": 0,
			"advance": 1,
			"ratio": 0
		})
	sequence.append({
		"idata": 0,
		"advance": section_length*advance_rate,
		"ratio": 0
	})
	return {
		"quarter_millis": quarter_millis,
		"sequence": sequence
	}

def fast_staccato_echo(quarter_millis=672, section_length=1, play_length=None, quarter_divisions=5, advance_rate=1/4):
	"""
	echos first stream with intermittent silence (idata=1) between each
	@param quarter_millis: millis per quarter
	@param section_length: how many quarter_millis per section
	@param play_length: override number sections to play. If None then plays section_length. Is interesting to use values smaller than section_length
	@param quarter_divisions: number of divisions per quarter
	@param advance_rate: how much to advance idata=1 per sequence
	@return:
	"""
	sequence=[]
	series=fishguts.iloop(numpy.linspace(0, section_length, section_length*quarter_divisions))
	if play_length is None:
		play_length=section_length*quarter_divisions
	else:
		play_length*=quarter_divisions
	for v in xrange(play_length):
		v=series.next()
		sequence.append({
			"idata": 0,
			"offset": v,
			"ratio": 1.0/(quarter_divisions*1.4)
		})
		sequence.append({
			"idata": 1,
			"ratio": 1.0/(quarter_divisions*1)
		})
		sequence.append({
			"idata": 0,
			"offset": v+section_length,
			"ratio": 1.0/(quarter_divisions*1.4)
		})
		sequence.append({
			"idata": 1,
			"ratio": 1.0/(quarter_divisions*1)
		})
	sequence.append({
		"idata": 0,
		"advance": advance_rate*section_length,
		"ratio": 0
	})
	sequence.append({
		"idata": 1,
		"advance": advance_rate*section_length,
		"ratio": 0
	})
	return {
		"quarter_millis": quarter_millis,
		"sequence": sequence
	}

