"""
created: 11/18/15
@author: curt
"""
from __future__ import division

import fishguts
import numpy

def get_rhythm():
	sequence=[]
	quarter_millis=356/2
	divisions=64
	series=fishguts.isin(divisions, 0, 4, count=divisions/2, phase=numpy.pi*3/2)
	for v in series:
		sequence.append({
			"idata": 0,
			"ratio": 0.25,
			"offset": v
		})
		sequence.append({
			"idata": 1,
			"ratio": 0.25,
			"advance": 0.25
		})
	series=fishguts.isin(divisions, 4, 0, count=divisions/2, phase=numpy.pi*3/2)
	for v in series:
		sequence.append({
			"idata": 0,
			"ratio": 0.25,
			"offset": v,
			"direction": "b"
		})
		sequence.append({
			"idata": 1,
			"ratio": 0.25,
			"advance": 0.25
		})
	sequence.append({
		"idata": 0,
		"ratio": 0,
		"advance": 4
	})
	for _ in range(1):
		series=numpy.linspace(0, 4, num=divisions)
		for v in series:
			sequence.append({
				"idata": 0,
				"ratio": 0.25,
				"offset": v
			})
			sequence.append({
				"idata": 1,
				"ratio": 0.25,
				"advance": 0.25
			})
		sequence.append({
			"idata": 0,
			"ratio": 0,
			"advance": 4
		})
		series=fishguts.ialternate(numpy.linspace(0, 4, num=divisions))
		for _ in range(divisions):
			v=series.next()
			sequence.append({
				"idata": 0,
				"ratio": 0.25,
				"offset": v
			})
			sequence.append({
				"idata": 1,
				"ratio": 0.25,
				"advance": 0.25
			})
		sequence.append({
			"idata": 0,
			"ratio": 0,
			"advance": 4
		})
	return {
		"quarter_millis": quarter_millis,
		"sequence": sequence
	}

