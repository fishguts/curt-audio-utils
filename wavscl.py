#!/usr/bin/env python

"""
Generates a equally tempered scale of x divisions and renders an PCM output file
created: 8/29/15
@author: curt
"""

import numpy
import os
import pydash
import traceback

from fishguts import cmdline
from fishguts import log
from fishguts import audio
from fishguts.audio import afile
from fishguts.audio import asequence


class Commander:
	def __init__(self, ):
		self.fout=None
		self.freq=220
		self.wform="sin"
		self.srate=22050
		self.samp=0x7fff/4*3
		self.rhythm="490,10"
		self.adsr={
			"attack": 1.0/5.0,
			"decay": 1.0/5.0
		}
		self.divisions=12
		self.sequence=None

	@staticmethod
	def usage():
		print "usage: wavscl.py [-v] [-sr sample_rate:22050] [-sa amplitude:~24575] [-f base-freq:220] [-d divisions:12] [-s sequence:(<index>,...)] [-r rhythm:<490,50>] [output file]"
		print "  -v:  verbose"
		print "  -sr: sample rate"
		print "  -sa: amplitude in sample units"
		print "  -f:  base frequency"
		print "  -d:  number of divisions within an octave"
		print "  -s:  sequence of tones indexes to generate. defaults to '0-<divisions>'"
		print "  -r:  rhythm milliseconds: [<on,off,on...]>"
		print "       the value must be a string that can be eval'ed. Further each element may be a string that can be eval'ed"


	def setup(self):
		"""
		Parse command line and get us ready for action
		@throws Exception if we just can't go on
		"""
		options, position=cmdline.parse(0, ["v"], ["sr", "sa", "f", "d", "s", "r"])
		log.set_verbose("v" in options)
		# parse
		self.fout=position[0] if len(position)>0 else "a.wav"
		self.srate=int(options.get("sr", self.srate))
		self.samp=int(options.get("sa", self.samp))
		self.freq=float(options.get("f", self.freq))
		self.divisions=int(options.get("d", self.divisions))
		self.sequence=options.get("s", self.sequence)
		self.rhythm=eval(options.get("r", self.rhythm))
		if self.sequence is None:
			self.sequence=range(0, self.divisions+1)
		else:
			self.sequence=eval(self.sequence)
		# validation
		if not pydash.is_iterable(self.rhythm):
			raise Exception("rhythm is not iterable")
		if not pydash.is_iterable(self.sequence):
			raise Exception("sequence is not iterable")

	def execute(self):
		data=self._preprocess_input()
		self._generate_stream(data)
		self._write_output(data)

	#--- private interface---#
	def _preprocess_input(self):
		frequency=lambda division: self.freq*pow(2, division/float(self.divisions))
		data={
			"scale": map(frequency, range(0, self.divisions+1)),
			"sequence":[],
			"sattack":[],
			"sdecay":[],
			"srhythm":[]
		}
		# generate attack, decay and rhythms
		for index in range(0, len(self.sequence)):
			srhythm_on=audio.millis_to_samples(self.srate, self.rhythm[(index*2)%len(self.rhythm)])
			srhythm_off=audio.millis_to_samples(self.srate, self.rhythm[(index*2+1)%len(self.rhythm)])
			data["sequence"].append(frequency(self.sequence[index]))
			data["sattack"].append(numpy.ceil(srhythm_on*self.adsr["attack"]))
			data["sdecay"].append(numpy.ceil(srhythm_on*self.adsr["decay"]))
			data["srhythm"].append(srhythm_on)
			data["srhythm"].append(srhythm_off)
		return data


	def _generate_stream(self, data):
		"""
		Generates sample buffer
		@param data: description of sequence
		"""
		data["stream"]=asequence.generate_stream(srate=self.srate, data=data)


	def _write_output(self, data):
		log.write_verbose("writing sequence {0} to {1}", map(round, data["sequence"]), self.fout)
		path=os.path.relpath(self.fout)
		samples=data["stream"]
		samples=numpy.array(samples*(self.samp/samples.max()), dtype=numpy.int16)
		afile.write(path=path, srate=self.srate, samples=samples)


#--- Action ---#
captain=Commander()
try:
	captain.setup()
except Exception as exception:
	log.write_error(exception)
	captain.usage()
	exit(1)

try:
	captain.execute()
except Exception as exception:
	log.write_failed(exception)
	traceback.print_exc()
	exit(1)

