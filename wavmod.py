#!/usr/bin/env python

"""
Modulates one wave file by another and writes the results to designated file or a.wav
created: 5/23/15
@author: curt
"""

import os
import numpy as np

from fishguts import cmdline
from fishguts import log
from fishguts.audio import afile
from fishguts.audio import axform


#--- Globals ---#
options=None
file_path_in=["",""]
file_path_out=None
mod_rate=None


#--- Support ---#
def setup():
	"""
	Parse the command line and setup environment variables
	@return: True if successful and False if failed
	"""
	try:
		global options, file_path_in, file_path_out, mod_rate
		options, position=cmdline.parse(2, ["v"], ["r"])
		log.set_verbose("v" in options)
		file_path_in[0]=os.path.abspath(position[0])
		file_path_in[1]=os.path.abspath(position[1])
		file_path_out=os.path.abspath(position[2]) if len(position)>2 else os.path.abspath("a.wav")
		mod_rate=float(options["r"]) if "r" in options else 1.0
	except Exception as exception:
		log.write_error(exception)
		print "usage: wavmod.py [-v] [-r rate] <input1.wav> <input2.wav> [output.wav]"
		print "  -v: verbose"
		print "  -r: modulation rate at which scalar samples are consumed - input2.wav/rate"
		return False
	return True


def execute():
	"""
	Loads source and scalar and normalizes scalar and the mods source by scalar.
	"""
	data=[None,None]	# [(srate,samples), (srate,samples)]
	for i in range(0, 2):
		data[i]=afile.read(file_path_in[i])
		log.write_verbose("read {0}: sample_rate={1}, sample_count={2}", os.path.relpath(file_path_in[i]), data[i][0], len(data[i][1]))
	# normalize the scalar - note: intentionally scaling first and then offsetting otherwise we easily overflow
	scalar_max=max(data[1][1].max(), abs(data[1][1].min()))
	scalar_normal=np.asfarray(data[1][1])/(scalar_max*2)
	scalar_normal-=scalar_normal.min()
	result=axform.modulate(data[0][1], scalar_normal, mod_rate)
	write_wav(data[0][0], result)


def write_wav(srate, samples):
	log.write_verbose("writing '{0}': duration={1:.2f}s", os.path.relpath(file_path_out), len(samples)/float(srate))
	afile.write(file_path_out, srate, samples)


#--- Action ---#
if not setup():
	exit(1)
else:
	try:
		execute()
	except Exception as exception:
		log.write_failed(exception)


