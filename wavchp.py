#!/usr/bin/env python

"""
Chops up a wav dividing it by detected silence.
 - todo: assumes a single channel
 - todo: default silence threshold is based on 16 bit sample encoding
@author: curt
@created: 5/10/2015
"""

import os
from fishguts import cmdline
from fishguts import log
from fishguts.audio import afile

# Globals
file_path_target=None
file_path_source=None
min_silence=(0,0)	# (sample_count, second_duration)
min_sound=(0,0)		# (sample_count, second_duration)
sample_increment=0
silence_threshold=0


# Functional support
def setup():
	"""
	Parse the command line and setup environment variables
	@return: True if successful and False if failed
	"""
	try:
		global file_path_target, file_path_source
		global min_silence, min_sound, silence_threshold
		options, position=cmdline.parse(1, ["v"], ["s", "S", "t"])

		file_path_source=os.path.abspath(position[0])
		file_name_base=os.path.splitext(os.path.split(file_path_source)[1])[0]
		if len(position)<2:
			file_path_target=os.path.join(os.path.split(file_path_source)[0], file_name_base)
		else:
			file_path_target=os.path.join(os.path.abspath(position[1]), file_name_base)
		min_silence=float(options["s"]) if "s" in options else 0.2
		min_sound=float(options["S"]) if "S" in options else 0.5
		silence_threshold=int(options["t"]) if "t" in options else 500
		log.set_verbose("v" in options)
	except Exception as exception:
		log.write_error(exception)
		print "usage: wavchp.py [-v] [-s silence_duration] [-S sound_duration] [-t silence_threshold] <input.wav> [target dir]"
		print "  -v: verbose"
		print "  -s: min silence duration in seconds"
		print "  -S: min sound duration in seconds"
		print "  -t: silence threshold in sample units (positive)"
		return False
	return True


def execute():
	"""
	Loads file, finds silences and writes results to target directory.
	"""
	global sample_increment, min_sound, min_silence
	srate, samples=afile.read(file_path_source)
	sample_increment=int(srate*.01)
	# now that we know the sample rate translate these guys into tuples of: (sample_count, duration)
	min_silence=(int(min_silence*srate), min_silence)
	min_sound=(int(min_sound*srate), min_sound)
	silences=get_silences(samples)
	split_silences(srate, samples, silences)


def get_silences(samples):
	"""
	Gets array of tuples of all sections of silence among samples
	@param samples : {array} of all samples
	@return : list or tuples of (sample_offset, sample_length)
	"""
	result=[]
	sample_offset=0
	sample_count=len(samples)
	while sample_offset<sample_count:
		count=get_silence(samples, sample_offset)
		# 1. if > min_silence then good
		# 2. if silence is at the beginning then allow it to be trimmed free regardless of how long it is
		# 3. if silence is at the end then allow it to be trimmed free regardless of how long it is
		if count>=0 and (count>=min_silence[0] or sample_offset==0 or sample_offset==sample_count-count):
			result.append((sample_offset, count))
			sample_offset+=count+min_sound[0]
		else:
			sample_offset+=sample_increment
	return result


def get_silence(samples, offset):
	"""
	Will report the number of samples considered silent starting from offset. Works at sample_minimum intervals
	@param samples: numpy array of all samples
	@param offset: index to start look from
	@return: duration in samples
	"""
	duration=0
	for index in xrange(offset, len(samples), sample_increment):
		subarray=samples[index:index+sample_increment]
		if subarray.max()<silence_threshold and subarray.min()>0-silence_threshold:
			duration+=sample_increment
		else:
			break
	return duration


def split_silences(srate, samples, silences):
	"""
	Splits up silences according to list of silence tuples in silences
	@param srate:
	@param samples:
	@param silences:
	"""
	silence_count=len(silences)
	log.write_verbose("found {0} silences in '{1}'", silence_count, os.path.relpath(file_path_source))
	if silence_count>0:
		file_index=1
		# todo: there are a couple or few ways to handle intervals that are below the min_sound sample count:
		#	- do we throw it away?
		#	- do we gather adjacent ones until we've reached a min?
		#	- being that we have the throw away, let's stick with it and see how it holds up.
		# if the first silence begins after the first sample then write the beginning
		if silences[0][0]>min_sound[0]:
			write_file(srate, samples, 0, silences[0][0], file_index)
			file_index+=1
		# all from last to index
		for index in xrange(1, silence_count):
			sample_end=silences[index][0]
			sample_start=silences[index-1][0]+silences[index-1][1]
			assert(sample_end-sample_start>=min_sound[0])
			write_file(srate, samples, sample_start, sample_end, file_index)
			file_index+=1
		# if the last silence ends before the end then write the end
		sample_start=silences[silence_count-1][0]+silences[silence_count-1][1]
		if sample_start<len(samples)-min_sound[0]:
			write_file(srate, samples, sample_start, len(samples), file_index)


def scale_samples(samples, sample_start, sample_end, ratio_from, ratio_to):
	"""
	Scales samples in specified range
	@param samples: numpy array of all samples
	@param sample_start: first sample index
	@param sample_end: up until sample index
	@param ratio_from: scale ratio from
	@param ratio_to: scale ratio to
	"""
	for index in xrange(sample_start, sample_end):
		ratio_offset=float(index-sample_start)/float(sample_end-sample_start)
		samples[index]*=ratio_from*(1-ratio_offset) + ratio_to*ratio_offset

def write_file(srate, samples, sample_start, sample_end, file_index):
	sample_count_fade=sample_increment/2
	file_path=file_path_target+"-%02d.wav"%file_index
	# fade in and out our ends so we don't pop
	scale_samples(samples, sample_start, sample_start+sample_count_fade, 0, 1)
	scale_samples(samples, sample_end-sample_count_fade, sample_end, 1, 0)
	log.write_verbose(" - writing '{0}': start={1:.2f}s, end={2:.2f}s, duration={3:.2f}s",
		os.path.relpath(file_path), sample_start/float(srate), sample_end/float(srate), (sample_end-sample_start)/float(srate))
	afile.write(file_path, srate, samples[sample_start:sample_end])


#--- Action ---#
if not setup():
	exit(1)
else:
	try:
		execute()
	except Exception as exception:
		log.write_failed(exception)


