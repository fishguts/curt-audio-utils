"""
created: 11/18/15
@author: celsasse
"""
import numpy

def get_rhythm():
	result=[]
	for ipart in range(1, 5):
		result.append({
			"quarter_millis": 100*ipart,
			"sequence": [ {
				"offset": randomf(0, 1),
				"advance": 0.5
			} for _ in range(4)]
		})
	return result
