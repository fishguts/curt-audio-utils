"""
created: 11/18/15
@author: celsasse
"""
import numpy

def get_rhythm():
	result={
		"quarter_millis": 1000,
		"sequence": [{
			"advance": 0.25,
			"attack": 0.5,
			"decay": 0.5
		}]
	}
	return result
