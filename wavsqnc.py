#!/usr/bin/env python

"""
Generates one or more sequences from a JSON description of a sequence
 writes output to file[s] or stdout
created: 8/16/2015
@author: curt
"""
import os
import random
import re

import numpy
import pydash
import sys

import fishguts
from fishguts import audio
from fishguts import cmdline
from fishguts import log
from fishguts.audio import afile
from fishguts.audio import asequence


class Commander:
	def __init__(self, ):
		self.input=None
		self.mix="none"
		self.fid=None
		self.fic=None
		self.fout=None
		self.wform="sin"
		self.srate=22050
		self.samp=0.75
		self.rhythm=(500,0)
		self.adsr={
			"attack": 50,
			"decay": 50
		}
		self.mixdown_buffer=None

	#--- public api ---#
	@staticmethod
	def usage():
		print "usage: wavsqnc.py [-v] [-m mix:none][-sr sample_rate:22050] <-fid input_data=stdin> <-fic input_configuration> [output_file/prefix:out.wav]"
		print "  -v:  verbose"
		print "  -m:  mix streams: mono, fan"
		print "  -fid: input data file"
		print "  -fic: input configuration file"
		print "  -sr: sample rate"
		print "  output_file/prefix: path or per stream prefix. default=a.wav"


	def setup(self):
		"""
		Parse command line and get us ready for action
		@throws Exception if we just can't go on
		"""
		options, position=cmdline.parse(0, ["v"], ["m", "fid", "fic", "sr"])
		log.set_verbose("v" in options)
		# parse and validate
		self.mix=fishguts.get(options, "m", self.mix)
		fishguts.raise_if_false(pydash.contains(["none", "mono", "fan"], self.mix), "unknown mix method '{0}'".format(self.mix))
		self.fid=fishguts.get(options, "fid", sys.stdin)
		self.fic=fishguts.get(options, "fic", required=True, error="must specify configuration file (-fic)")
		self.fout=fishguts.get(position, 0, "a.wav" if self.mix!="none" else "")
		self.srate=int(options.get("sr", self.srate))

	def execute(self):
		self._read_input()
		self._preprocess_input()
		self._generate_streams()


	#--- private interface---#
	def _read_input(self):
		"""
		Reads input file or stdin and parses contents (assuming JSON)
		@return: parsed input data
		"""
		self.input={}
		log.write_verbose("reading {0}", fishguts.path_to_name(self.fid))
		self.input["data"]=fishguts.path_to_json(self.fid)
		log.write_verbose("reading {0}", self.fic)
		self.input["config"]=fishguts.path_to_json(self.fic)

	def _preprocess_input(self):
		self._preprocess_instrument()
		self._preprocess_melody()
		self._preprocess_rhythm()

	def _preprocess_instrument(self):
		log.write_verbose("preprocessing instruments")
		meta=fishguts.get(self.input, "config.instrument")
		self.adsr["attack"]=fishguts.get(meta, "attack", self.adsr["attack"])
		self.adsr["decay"]=fishguts.get(meta, "decay", self.adsr["decay"])
		self.wform=fishguts.get(meta, "waveform", self.wform)
		self.samp=fishguts.get(meta, "amplitude", 0.75)*0x7fff

	def _preprocess_melody(self):
		log.write_verbose("preprocessing sequence data")
		space=self._config_to_freq_space()
		datas=fishguts.get(self.input, "data.data", [])
		for data in datas:
			normals=data["sequence"]
			data["sequence"]=map(lambda normal: space[min(len(space)-1, int(normal*len(space)))], normals)

	def _config_to_freq_space(self):
		meta=fishguts.get(self.input, "config.scale")
		algorithm=fishguts.get(meta, "algorithm", "diatonic")
		if algorithm=="literal":
			return fishguts.get(meta, "space", required=True, error="missing scale space")
		else:
			space=[]
			divisions=fishguts.get(meta, "divisions", 12)
			if algorithm=="diatonic":
				numerators=[0,2,4,5,7,9,11]
			elif algorithm=="chromatic":
				numerators=range(12)
			elif algorithm=="micro":
				numerators=range(divisions)
			elif algorithm=="expression":
				expression=fishguts.get(meta, "expression", required=True, error="missing scale expression")
				try:
					numerators=eval(expression)
					assert(pydash.is_iterable(numerators))
				except:
					raise Exception("unparsable scale expression '{0}'".format(meta["expression"]))
			else:
				raise ValueError("unknown scale algorithm '{0}'".format(algorithm))
			for octave in range(meta.get("octaves", 12)):
				for numerator in numerators:
					space.append(meta.get("frequency", 220)*pow(2, octave+float(numerator)/divisions))
			return tuple(space)


	def _preprocess_rhythm(self):
		"""
		config["rhythm"] may have zero or more configurations from which, during configuration, each series
		is assigned one of the configurations which is used to calculate the streams rhythm.  There are various
		different types of rhythm algorithms supported: algorithmic, expression|literal, clone, cycle, complement.
		See _config_to_rhythms for more details
		@return: None
		"""
		log.write_verbose("preprocessing rhythm:")
		rhythms=self._config_to_rhythms()
		datas=fishguts.get(self.input, "data.data", [])
		assert(len(rhythms)==len(datas))
		for iseries in xrange(len(datas)):
			log.write_verbose(" - calculating sequence '{0}'", datas[iseries].get("name", iseries))
			rindex=-1
			data=datas[iseries]
			rhythm=rhythms[iseries]
			data["iseries"]=iseries
			data["sattack"]=[]
			data["sdecay"]=[]
			data["rhythm"]=[]
			sequence=data["sequence"]
			for itone in xrange(len(sequence)):
				def _eval(expression, clone=False):
					if pydash.is_string(expression):
						expression=eval(expression, {
							"iseries": iseries,
							"lseries": len(datas),
							"itone": itone,
							"ltone": len(sequence),
							"sin": numpy.sin,
							"log": numpy.log10,
							"sqrt": numpy.sqrt
						})
					elif clone:
						expression=pydash.clone(expression)
					return expression
				def _eval_to_samples(expression):
					return audio.millis_to_samples(srate=self.srate, millis=_eval(expression))

				data["sattack"].append(_eval_to_samples(self.adsr["attack"]))
				data["sdecay"].append(_eval_to_samples(self.adsr["decay"]))
				# little strange but it makes sure that we don't endlessly spin our wheels if there is no object with "on"
				for rindex in range(rindex+1, rindex+1+len(rhythm)):
					value=_eval(rhythm[rindex%len(rhythm)], True)
					data["rhythm"].append(value)
					if "off" in value:
						value["off"]=audio.millis_to_samples(srate=self.srate, millis=value["off"])
					elif "on" in value:
						value["on"]=audio.millis_to_samples(srate=self.srate, millis=value["on"])
						break

	def _config_to_rhythms(self):
		"""
		Processes config["rhythm"] and then creates one rhythm description for every data sequence
		@return: tuple of rhythms mapped by index to each data sequence
		"""
		metas=self._preprocess_rhythm_metas()
		# read and transform each description into a rhythm sequence
		for mindex in range(len(metas)):
			metas[mindex]["rhythm"]=self._rhythm_meta_to_rhythm(metas, mindex)
		# create a mapping of one to one for each of our data sequences
		mapped=[]
		datas=fishguts.get(self.input, "data.data", [])
		for sindex in range(len(datas)):
			for meta in metas:
				# look for a lambda function for sequence to rhythm mapping
				if meta.get("sequence") is not None and eval(meta["sequence"])(sindex, len(datas)):
					mapped.append(meta["rhythm"])
					break
			# if we didn't find a match then use a simple revolving mapping
			if len(mapped)==sindex:
				mapped.append(metas[sindex%len(metas)]["rhythm"])
		return tuple(mapped)

	def _rhythm_meta_to_rhythm(self, metas, index):
		"""
		Translates a rhythm description into a sequence of {"on":x}, {"off":y} objects or sequence of equations
		@param metas: list of meta descriptions
		@param index: to transform into sequence
		@return: iterable sequence
		"""
		meta=metas[index]
		algorithm=fishguts.get(meta, "algorithm")
		# rhythms can have two levels of depth so we are liberal in terms of how they are classified
		if algorithm in ["literal", "expression"]:
			rhythm=meta.get("literal") if "literal" in meta else meta.get("expression")
			try:
				# - may be an array of strings or values
				# - may be a string that may be eval'ed to an array of strings or or {"on":x}, {"off":y} objects
				if pydash.is_string(rhythm):
					rhythm=eval(rhythm)
				assert(pydash.is_iterable(rhythm))
			except:
				raise Exception("unparsable rhythm expression '{0}'".format(meta["expression"]))
		elif algorithm=="clone":
			if index==0:
				raise Exception("clone must have a previous rhythm to act upon")
			rhythm=metas[index-1]["rhythm"]
		elif algorithm=="complement":
			if index==0:
				raise Exception("complement must have a previous rhythm to act upon")
			rhythm=[]
			for value in metas[index-1]["rhythm"]:
				if pydash.is_string(value):
					complemented=re.sub("['\"]on['\"]", "'off'", value)
					if complemented==value:
						complemented=re.sub("['\"]off['\"]", "'on'", value)
					rhythm.append(complemented)
				else:
					rhythm.append({"off":value["on"]} if "on" in value else {"on":value["off"]})
		else:
			raise ValueError("unknown rhythm algorithm '{0}'".format(algorithm))
		return rhythm

	def _preprocess_rhythm_metas(self):
		"""
		Preprocess rhythm descriptions and returns a complete list of configurations. There are some configuration
		descriptions which this guys treats specially (such as "algorithmic" and "cycle"). These are configurations
		that may be used to generate one or more rhythm profiles.
		@return: tuple of rhythm profiles
		"""
		result=[]
		metas=fishguts.get(self.input, "config.rhythm")
		metas=(metas,) if pydash.is_dict(metas) else metas
		for meta in metas:
			algorithm=fishguts.get(meta, "algorithm")
			if algorithm=="algorithmic":
				result.extend(self._rhythm_algorithmic_to_metas(meta))
			elif algorithm=="cycle":
				result.extend(self._rhythm_cycle_to_metas(meta))
			else:
				result.append(meta)
		return tuple(result)

	def _rhythm_algorithmic_to_metas(self, meta):
		"""
		translates algorithmic description of a configuration into one or more "literal" rhythm profiles
		@return: tuple of rhythm profiles
		"""
		_meta={
			"algorithm": "literal",
			"sequence": meta.get("sequence")
		}
		rhythm=_meta["literal"]=[]
		erratic=float(meta.get("erratic", 0.5))
		activity=float(meta.get("activity", 0.5))
		silence=float(meta.get("silence", 0.5))
		quarter=float(meta.get("quarter", 500))
		quarters=int(meta.get("quarters", 4))
		divisions=int(meta.get("divisions", 2))
		fishguts.raise_if_false(0<=erratic<=1, "erratic expected to normal value")
		fishguts.raise_if_false(0<=activity<=1, "activity expected to normal value")
		fishguts.raise_if_false(0<=silence<=1, "silence expected to be normal value")
		fishguts.raise_if_false(quarters>=0, "quarters must be greater than 0")
		def _generate(into, level, weight=0.0):
			if level<divisions and (activity+weight)>=random.random():
				weight=random.random()*erratic*level
				_generate(into, level+1, 0+weight)
				_generate(into, level+1, 0-weight)
			elif (silence-weight)>random.random():
				into.append({"off": quarter/pow(2, level)})
			else:
				into.append({"on": quarter/pow(2, level)})
			return into
		# build our list
		for iquarter in range(quarters):
			rhythm.extend(_generate([], 0))
		# if we didn't create any on's then switch the first guy
		if not any(map(lambda o: "on" in o, rhythm)):
			rhythm[0]["on"]=rhythm[0]["off"]
			del rhythm[0]["off"]
		return (_meta,)

	def _rhythm_cycle_to_metas(self, meta):
		"""
		Translate a cycle rhythm description into one or more rhythm profiles
		@return: tuple of rhythm profiles
		"""
		result=[]
		sequences=fishguts.get(self.input, "data.data", [])
		count=int(meta["count"]) if "count" in meta else len(sequences)
		options=fishguts.get(meta, "options", [])
		if "millis" in meta:
			millis_per_unit=float(meta["millis"])/count
		elif "millis_on" in meta:
			millis_per_unit=float(meta["millis_on"])
		else:
			raise ValueError("cycle must have a 'milli' or 'milli.on' value")
		millis_gap=fishguts.get(meta, "millis_gap", 0)
		for mindex in range(count):
			_meta={
				"algorithm": "literal",
				"sequence": meta.get("sequence"),
				"literal": [{"off":mindex*(millis_per_unit+millis_gap)}, {"on":millis_per_unit}, {"off":(count-mindex-1)*(millis_per_unit+millis_gap)}]
			}
			if "bounce" in options:
				_meta["literal"].extend(_meta["literal"][::-1])
			if _meta["sequence"] is None:
				if "block" in options:
					if "alternate" in options:
						# we alternate and work our way towards the center:
						#  - the first meta block is the first sequences/count number of sequences
						#  - the next block is the last sequences/count number of sequences. And then back to the first step.
						if mindex%2==0:
							_meta["sequence"]="lambda iseq, lseq: int(iseq*{0})=={1}".format(float(count)/len(sequences), mindex/2)
						else:
							_meta["sequence"]="lambda iseq, lseq: int(iseq*{0})=={1}".format(float(count)/len(sequences), count-mindex/2-1)
					else:
						_meta["sequence"]="lambda iseq, lseq: int(iseq*{0})=={1}".format(float(count)/len(sequences), mindex)
				else:
					# assume sequential and revolving
					if "alternate" in options:
						_meta["sequence"]="lambda iseq, lseq: iseq%{0}=={1}".format(count, mindex if mindex%2==0 else count-(mindex+1)/2)
					else:
						_meta["sequence"]="lambda iseq, lseq: iseq%{0}=={1}".format(count, mindex)
			result.append(_meta)
		return tuple(result)


	def _generate_streams(self):
		"""
		Translates textual descriptions of sequences into audio signal streams and persists them
		"""
		datas=fishguts.get(self.input, "data.data", [])
		log.write_verbose("generating audio streams from {0} sequences:", len(datas))
		for data in datas:
			samples=self._sequence_to_samples(data)
			if self.mix=="mono":
				self._mix_sequence_mono(data=data, samples=samples)
			elif self.mix=="fan":
				self._mix_sequence_fan(data=data, samples=samples)
			else:
				fishguts.raise_if_false(self.mix=="none", "expected mix='none'")
				self._write_sequence(data=data, samples=samples)
		if self.mixdown_buffer is not None:
			self._write_samples(name="mixed", path=os.path.relpath(self.fout), samples=self.mixdown_buffer)


	def _sequence_to_samples(self, data):
		"""
		Generates textual description of a sequences into an array of normalized audio samples
		@param data: json description of sequence
		@return: normalized sample buffer
		"""
		log.write_verbose(" - generating '{0}'", pydash.get(data, "name", "[unknown]"))
		return asequence.generate_stream(srate=self.srate, data=data)


	def _mix_sequence_mono(self, data, samples):
		log.write_verbose(" - mixing mono '{0}'", data["name"])
		if self.mixdown_buffer is None:
			self.mixdown_buffer=samples
		else:
			if samples.size>self.mixdown_buffer.size:
				self.mixdown_buffer.resize(samples.size)
			self.mixdown_buffer[0:samples.size]+=samples

	def _mix_sequence_fan(self, data, samples):
		log.write_verbose(" - mixing fan '{0}'", data["name"])
		iseries=data["iseries"]
		cseries=len(self.input["data"]["data"])
		balance=float(iseries)/float(cseries-1) if cseries>1 else 0.5
		if self.mixdown_buffer is None:
			self.mixdown_buffer=numpy.zeros((2, samples.size), samples.dtype)
		elif samples.size>self.mixdown_buffer.shape[1]:
			self.mixdown_buffer.resize((2, samples.size), refcheck=False)
		# 0-count/2 => biased to left
		self.mixdown_buffer[0][0:samples.size]+=samples*(1-balance)
		# count/2-count => biased to right
		self.mixdown_buffer[1][0:samples.size]+=samples*balance

	def _write_sequence(self, data, samples):
		prefix="" if self.fout is None else self.fout
		path=os.path.relpath(prefix+data["name"]+".wav")
		self._write_samples(name=data["name"], path=path, samples=samples)

	def _write_samples(self, name, path, samples):
		log.write_verbose(" - writing '{0}' to {1}", name, path)
		# convert to 16 bit ints and match our target amplitude
		samples16=numpy.array(samples*(self.samp/samples.max()), dtype=numpy.int16)
		afile.write(path=path, srate=self.srate, samples=samples16)


#--- Action ---#
captain=Commander()
try:
	captain.setup()
except Exception as exception:
	log.write_error(exception)
	captain.usage()
	exit(1)

try:
	captain.execute()
except Exception as exception:
	log.write_failed(exception)
	# traceback.print_exc()
	exit(1)

