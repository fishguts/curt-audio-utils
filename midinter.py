#!/usr/bin/env python

"""
Interpolates chord changes (note clusters) within a midi file and writes to interpolated version
created: 8/18/16
@author: curt
"""

from __future__ import division

import math
import os
import re
import traceback
from os import path

import midi
import pydash

import fishguts
from fishguts import bisect
from fishguts import cmdline
from fishguts import log


class Commander:
	"""
	Uses midi file as an outline and interpolates notes according to our rules
	"""
	power_regex=r"^power(\d+|\d+\.\d+|\.\d+)?$"
	spike_regex=r"^spike(\d+|\d+\.\d+|\.\d+)?$"

	def __init__(self):
		self.dir_app=os.path.dirname(os.path.realpath(__file__))
		self.options=None
		self.config=None
		self.path_source=None
		self.path_target=None
		self.midi_data=None
		self.archipelagos=[]

	# --- public interface---#
	@staticmethod
	def usage():
		print "usage: midinter.py [-v,-vv] [-c conf.json] <source.mid>"
		print "  -v: verbose"
		print "  -vv: very verbose"
		print "  -c: configuration file. Defaults to config/midinter_conf.json"
		print "  -g: grouping: global|track"
		print "  -t.r: transition rhythm: fixed|fixed+|linear|linear+"
		print "    * fixed: source notes are used for transitions"
		print "    * fixed+: source notes are used for transitions and notes are added to islands to make chord transitions complete"
		print "    * linear: duration changes are linear."
		print "    * linear+: duration changes are linear and notes are added to islands to make chord transitions complete"
		print "  -t.rc: transition curve: linear|power\d?|spike\d?"
		print "  -t.m: transition melody: fade"
		print "  -t.vc: transition curve: linear|power\d?|spike\d?"
		print "  -t.vm: transition from/to velocity minimum: 2"

	def setup(self):
		"""
		Reads the command line and setup environment variables
		@raise: if parsing fails
		"""
		curve_pattern=r"^linear$|{0}|{1}".format(self.power_regex, self.spike_regex)
		overrides=(
			# (option, path, optional validator)
			("g", "grouping"),
			("t.r", "transition.rhythm", lambda v: pydash.contains(("fixed", "fixed+", "linear", "linear+"), v)),
			("t.rc", "transition.rhythmCurve", lambda v: re.match(curve_pattern, v) is not None),
			("t.m", "transition.melody"),
			("t.vc", "transition.velocityCurve", lambda v: re.match(curve_pattern, v) is not None),
			("t.vm", "transition.velocityMin", lambda v: 128<v<0)
		)
		self.options, position=cmdline.parse(1, ["v", "vv"], ["c"]+[override[0] for override in overrides])
		log.set_verbose("v" in self.options, "vv" in self.options)
		log.write_verbose("processing command line")
		self.path_source=position[0]
		if len(position)>1:
			self.path_target=position[1]
		else:
			self.path_target="{0}-midinter{1}".format(*path.splitext(self.path_source))
		self.config=fishguts.path_to_json(os.path.join(self.dir_app, "./config/midinter_conf.json"))
		if "c" in self.options:
			pydash.merge(self.config, fishguts.path_to_json(self.options["c"]))
		# apply option overrides directly to self.config
		for override in overrides:
			if override[0] in self.options:
				self.config=pydash.set_(self.config, override[1], self.options[override[0]])
				# validation
				if len(override)>2:
					fishguts.raise_if_false(override[2](self.options[override[0]]),
						"-{0}: invalid value '{1}'".format(override[0], self.options[override[0]]))


	def execute(self):
		"""
		Sequences operations and then mixes them according to env configuration
		"""
		self._load_files()
		self._build_archipelagos()
		self._generate_target()
		self._write_results()

	# --- private api ---#
	def _load_files(self):
		"""
		Loads midi file and all dependent audio files
		@return: None
		"""
		log.write_verbose("loading {0}:", os.path.relpath(self.path_source))
		self.midi_data=midi.read_midifile(self.path_source)

	def _write_results(self):
		log.write_verbose("writing {0}", os.path.relpath(self.path_target))
		midi.write_midifile(self.path_target, self.midi_data)

	def _build_archipelagos(self):
		if self.config["grouping"]=="global":
			self.archipelagos.append(self._build_archipelago(self.midi_data))
		else:
			fishguts.raise_if_false(self.config["grouping"]=="track", "unknown grouping '{0}'".format(self.config["grouping"]))
			# create a island_track for each track
			for i in range(len(self.midi_data)):
				self.archipelagos.append(self._build_archipelago(self.midi_data[i:i+1]))
		self._process_archipelagos()

	def _build_archipelago(self, tracks):
		islands=[]
		sustains=[]
		for it in range(len(tracks)):
			track=tracks[it]
			for event in track:
				if isinstance(event, midi.ControlChangeEvent) and event.control==64:
					if event.value>0:
						sustains.append({
							"start": event.offset,
							"stop": event.offset
						})
					elif len(sustains)>0 and sustains[-1]["start"]==sustains[-1]["stop"]:
						sustains[-1]["stop"]=event.offset
				elif isinstance(event, midi.NoteEvent):
					quantized=self.midi_data.nearest_quantized_duration(event.duration)
					ii=bisect.bisect_right(islands, event.offset, vof=lambda island: island["start"])
					islands.insert(ii, {
						"start": event.offset,
						"stop": event.offset+event.duration,
						"sustain": False,
						"data": pydash.set_({}, "{0}:{1}".format(it, event.pitch), {
							"event": event,
							"track": track,
							"quantized": quantized
						})
					})
		# merge adjacent, overlapping islands together
		ii=1
		while ii<len(islands):
			if islands[ii-1]["stop"]<=islands[ii]["start"]:
				ii+=1
			else:
				islands[ii-1]["stop"]=max(islands[ii-1]["stop"], islands[ii]["stop"])
				pydash.merge(islands[ii-1]["data"], islands[ii]["data"], _clone=False)
				del islands[ii]
		# process sustains
		for sustain in sustains:
			for island in islands:
				if sustain["stop"]<=island["start"]:
					break
				elif sustain["start"]<island["stop"] and sustain["stop"]>island["stop"]:
					island["sustain"]=True
		return islands

	def _process_archipelagos(self):
		def _bridge_islands(island_source, island_populate, purpose):
			missing=pydash.omit(island_source["data"], island_populate["data"].keys())
			# we just select a random source item as a role model
			sampled_data=island_populate["data"].itervalues().next()
			for key, source_data in missing.iteritems():
				if source_data.get("purpose")=="bridge":
					continue
				event=midi.NoteOnEvent(
					channel=source_data["event"].channel,
					pitch=source_data["event"].pitch,
					offset=island_populate["start"],
					duration=sampled_data["event"].duration,
					velocity=source_data["event"].velocity if purpose=="sustain" else self.config["transition"]["velocityMin"])
				sampled_data["track"].insert_event(event)
				island_populate["data"][key]={
					"event": event,
					"track": sampled_data["track"],
					"quantized": sampled_data["quantized"],
					"purpose": purpose
				}

		if pydash.ends_with(self.config["transition"]["rhythm"], "+"):
			for archipelago in self.archipelagos:
				for i in range(1, len(archipelago)):
					# retire/sustain notes
					_bridge_islands(archipelago[i-1], archipelago[i], "sustain" if archipelago[i-1]["sustain"] else "bridge")
					# enlist notes
					_bridge_islands(archipelago[i], archipelago[i-1], "bridge")

	def _generate_target(self):
		for archipelago in self.archipelagos:
			for index in range(1, len(archipelago)):
				self._transition_notes(archipelago[index-1], archipelago[index])

	def _transition_notes(self, island1, island2):
		algorithm=pydash.pick(island1["data"], island2["data"].keys())
		for note_key, note_value in algorithm.iteritems():
			note_data_from=island1["data"][note_key]
			note_data_to=island2["data"][note_key]
			note_from=note_data_from["event"]
			note_to=note_data_to["event"]
			velocity_curve=self._get_curve(self.config["transition"]["velocityCurve"], note_from.velocity, note_to.velocity)
			if self.config["transition"]["rhythm"].startswith("linear"):
				sequence=self._generate_duration_sequence_transition(note_data_from, note_data_to)
			else:
				sequence=self._generate_duration_sequence_constant(note_from.offset, note_to.offset, note_data_from)
			for data in sequence[1:]:
				velocity=velocity_curve((data["offset"]-note_from.offset)/(note_to.offset-note_from.offset))
				fishguts.raise_if_false(velocity>0, "velocity should be > 0")
				note_value["track"].insert_event(midi.NoteOnEvent(
					channel=note_from.channel,
					offset=data["offset"],
					duration=data["duration"],
					pitch=note_from.pitch,
					velocity=velocity))

	def _generate_duration_sequence_constant(self, tick_from, tick_to, note_data):
		"""
		generates sequence: [tick_from, tick_to)
		@return: [{"offset": int, "duration": int}]
		"""
		return [{
					"offset": tick_from+i*note_data["quantized"],
					"duration": note_data["event"].duration
				} for i in range(int((tick_to-tick_from)/note_data["quantized"]))]

	def _generate_duration_sequence_transition(self, note_data_from, note_data_to):
		"""
		generates sequence: [note_data_from["event"].offset, note_data_to["event"].offset)
		@return: [{"offset": int, "duration": int}]
		"""
		if note_data_from["quantized"]==note_data_to["quantized"]:
			result=self._generate_duration_sequence_constant(note_data_from["event"].offset, note_data_to["event"].offset, note_data_from)
		else:
			sum_of_series=lambda n: (n+1)*n/2
			curve=self._get_curve(self.config["transition"]["rhythmCurve"], note_data_from["event"].duration, note_data_to["event"].duration)
			# find the best fit for number of divisions.  This is pretty much an area under the curve problem. Could use simplification.
			result=[{
				"offset": note_data_from["event"].offset,
				"duration": note_data_from["quantized"]
			}]
			while result[-1]["offset"]+result[-1]["duration"]<note_data_to["event"].offset:
				offset_ratio=(result[-1]["offset"]+result[-1]["duration"]-note_data_from["event"].offset)/(note_data_to["event"].offset-note_data_from["event"].offset)
				duration=max(1, curve(offset_ratio))
				result.append({
					"offset": result[-1]["offset"]+result[-1]["duration"],
					"duration": duration
				})
			# see which got us closer to the target: last or next to last
			if len(result)>1 and note_data_to["event"].offset-result[-2]["offset"]-result[-2]["duration"]<=result[-1]["offset"]+result[-1]["duration"]-note_data_to["event"].offset:
				result.pop()
			if len(result)>1:
				delta=(note_data_to["event"].offset-(result[-1]["offset"]+result[-1]["duration"]))
				delta_count=sum_of_series(len(result)-1)
				for i in range(len(result)):
					duration_adjustment=i/delta_count*delta
					result[i]["duration"]=int((result[i]["duration"]+duration_adjustment)*(note_data_from["quantized"]/note_data_from["event"].duration))
					if i>0:
						result[i]["offset"]=int(result[i-1]["offset"]+result[i-1]["duration"]*(note_data_from["event"].duration/note_data_from["quantized"]))
		return result

	def _get_curve(self, name, vfrom, vto):
		if name=="linear":
			return self._curve_linear(vfrom, vto)
		elif name.startswith("power"):
			return self._curve_power(vfrom, vto, float(re.match(self.power_regex, name).groups(2)[0]))
		elif name.startswith("spike"):
			return self._curve_spike(vfrom, vto, float(re.match(self.spike_regex, name).groups(2)[0]))
		else:
			log.write_failed("curve '{0}'?", name)

	@staticmethod
	def _curve_linear(vfrom, vto):
		return lambda ratio: int(vfrom+(vto-vfrom)*ratio)

	@staticmethod
	def _curve_power(vfrom, vto, power=2):
		base=math.pow(math.fabs(vto-vfrom), 1/power)
		if vto>=vfrom:
			return lambda ratio: int(vfrom+math.pow(base*ratio, power))
		else:
			return lambda ratio: int(vfrom-math.pow(base*ratio, power))

	@staticmethod
	def _curve_spike(vfrom, vto, power=2):
		base=math.pow(math.fabs(vto-vfrom), 1/power)
		if vto>=vfrom:
			return lambda ratio: int(vfrom+math.pow(base*ratio, power))
		else:
			return lambda ratio: int(vto+math.pow(base*(1-ratio), power))


# --- Action ---#
captain=Commander()
try:
	captain.setup()
except Exception as exception:
	log.write_error(exception)
	captain.usage()
	exit(1)

try:
	captain.execute()
except Exception as exception:
	log.write_failed(exception)
	traceback.print_exc()
	exit(1)
