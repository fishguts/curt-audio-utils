#!/usr/bin/env python

"""
Mixes two source waves and writes results to designated file or a.wav
created: 5/23/15
@author: curt
"""

import os
import random
import traceback

import numpy
import pydash

import fishguts
from fishguts import audio
from fishguts import cmdline
from fishguts import log
from fishguts.audio import afile
from fishguts.audio import ainfo

class Commander:
	"""
	Mixes two source waves and writes results to designated file or a.wav
	"""

	def __init__(self):
		self.options=None
		self.file_path_in=["", ""]
		self.file_path_out=None
		self.datas=[]  # [(srate,samples), ...]
		self.sequences=[]
		self.attack=[0, 0]
		self.decay=[0, 0]
		self.globals={
			"pi": numpy.pi,
			"rangef": numpy.arange,
			"linspace": numpy.linspace,
			"sin": numpy.sin,
			"log": numpy.log10,
			"sqrt": numpy.sqrt,
			"randomi": random.randint,
			"randomf": random.uniform,
			"iloop": fishguts.iloop,
			"ibounce": fishguts.ibounce,
			"ialternate": fishguts.ibounce,
			"irandom": fishguts.irandom,
			"isin": fishguts.isin,
			"iexp": fishguts.iexp
		}


	# --- public interface---#
	@staticmethod
	def usage():
		print "usage: wavmix.py [-v,-vv] [-g <expression>] [-p 'fb'] [-a attack_millis] [-d decay_millis]"
		print "        [-d[i] total_divisions]"
		print "        [-i[i] sample_interval] [-I[i] second_interval]"
		print "        [-qa quarter_millis:ratio1, ratio2, ratio1...]"
		print "        [-qr quarter_millis:<ratio1 & ratio2>, <ratio1 & ratio2>...]"
		print "        [-qe quarter_millis:(ratio, <0|1> [,advance|'b'|'f'] [,'b'|'f']), ...]"
		print "        [-qef <path>]"
		print "        <input1.wav> <input2.wav> [output.wav]"
		print "  -v: verbose"
		print "  -vv: very verbose"
		print "  -g:  expression which will be evaluated and functions and variables will be put into a context used by all subsequent eval's"
		print "  -a:  attack in millis"
		print "  -d:  decay in millis"
		print "  -p: direction of sample blocks: f=forward, b=backward. eg. 'bf' => backward, forward, backward..."
		print "  -d[1|2]: total number of divisions (~crossings=divisions+1). mutually exclusive with other interval options"
		print "  -i[1|2]: min self.interval in samples between crossings. mutually exclusive with other interval options"
		print "  -I[1|2]: min self.interval in millis between crossings. mutually exclusive with other interval options"
		print "  -qa: alternating quantized rhythm: [quarter_in_millis:ratio_input[0], ratio_input[1], ratio_input[0], ...]"
		print "  	  ratio=multiple of quarter: 2=half, 1=quarter, .5=eight, .25=sixteenth"
		print "  -qr: repeated quantized rhythm: [quarter_in_millis:<ratio_input[0] & ratio_input[1]>, <ratio_input[0] & ratio_input[1]>, ...]"
		print "  	  ratio=multiple of quarter: 2=half, 1=quarter, .5=eight, .25=sixteenth"
		print "  -qe: sequenced arrangement: quarter_millis:(ratio, <0|1> [,advance|'b'|'f'] [,'b'|'f']), ...]"
		print "  	  ratio=multiple of quarter: 2=half, 1=quarter, .5=eight, .25=sixteenth"
		print "  	  <0|1>=the stream index"
		print "  	  [advance]=multiple of quarter by which to advance the stream after element is processed"
		print "  	  [b|f]=direction of sample block"
		print "  -qef: path based configuration. must have 'quarter_millis' value and 'get_sequence' function"


	def setup(self):
		"""
		Reads the command line and setup environment variables
		@raise: if parsing fails
		"""
		self.options, position=cmdline.parse(2, ["v", "vv"], ["g", "a", "d", "p", "d1", "d2", "i1", "i2", "I1", "I2", "qa", "qe", "qr", "qef"])
		log.set_verbose("v" in self.options, "vv" in self.options)
		log.write_verbose("processing command line")
		self.file_path_in[0]=os.path.abspath(position[0])
		self.file_path_in[1]=os.path.abspath(position[1])
		self.file_path_out=os.path.abspath(position[2]) if len(position)>2 else os.path.abspath("a.wav")
		# establish globals
		if "g" in self.options:
			self._eval(self.options["g"], True)
		# pattern
		if "qe" in self.options or "qef" in self.options:
			fishguts.raise_if_true("p" in self.options, "-qe[x] - pattern should be built into sequence")
		else:
			self.options["p"]=self.options["p"].lower() if "p" in self.options else "f"
			if len(self.options["p"])==0 or len(self.options["p"].strip("fb"))>0:
				raise Exception("invalid pattern string. supports 'f' and 'b'")
		# quantize options
		if "qa" in self.options:
			self._parse_option_qa()
		elif "qe" in self.options:
			self._parse_option_qe()
		elif "qef" in self.options:
			self._parse_option_qef()
		elif "qr" in self.options:
			self._parse_option_qr()


	def execute(self):
		"""
		Sequences operations and then mixes them according to env configuration
		"""
		self._load_files()
		self._config_instrument()
		self._config_sequence()
		result=self._mix()
		log.write_verbose("writing {0}: length={1:.2f}", os.path.relpath(self.file_path_out), len(result)/float(self.datas[0][0]))
		afile.write(self.file_path_out, self.datas[0][0], result)


	# --- private api ---#
	def _eval(self, code, _compile=False):
		"""
		Evals or compiles and evals an expression using ole 'eval' but adds to the global context.
		@param code:
		@return:
		"""
		try:
			if _compile:
				code=compile(code, "<string>", "exec")
			return eval(code, self.globals)
		except Exception as exception:
			raise Exception("attempt to eval '{0}' failed - {1}".format(code, exception))


	def _parse_option_qa(self):
		"""
		Alternating distribution. using option["Ix"] to do our work
		:return: None
		"""
		quarter_millis, sequence=self.options["qa"].split(":")
		quarter_millis=float(quarter_millis)
		sequence=self._eval(sequence)
		ratio_list=map(float, sequence)
		fishguts.raise_if_false(len(ratio_list)%2==0, "-qr - ratio list must be multiple of 2")
		self.options["I1"]=tuple([(ratio_list[i]*quarter_millis) for i in range(0, len(ratio_list), 2)])
		self.options["I2"]=tuple([(ratio_list[i]*quarter_millis) for i in range(1, len(ratio_list), 2)])


	def _parse_option_qr(self):
		"""
		Lists will be identical. using option["Ix"] to do our work
		:return: None
		"""
		quarter_millis, sequence=self.options["qr"].split(":")
		quarter_millis=float(quarter_millis)
		sequence=self._eval(sequence)
		ratio_list=map(float, sequence)
		self.options["I1"]=self.options["I2"]=tuple(map(lambda ratio:ratio*quarter_millis, ratio_list))


	def _parse_option_qe(self):
		quarter_millis, sequence=self.options["qe"].split(":")
		quarter_millis=float(quarter_millis)
		sequence=self._eval(sequence)
		self._process_rhythm_sequence(quarter_millis, sequence)
		fishguts.raise_if_false(pydash.any_(self.options["rhythm"], lambda item: item["idata"]==0),
			"rhythm sequence - must include at least one reference to the first stream")


	def _parse_option_qef(self):
		def _process_bar(bar):
			fishguts.raise_if_false("sequence" in bar, "'sequence' not found in bar")
			fishguts.raise_if_false("quarter_millis" in bar, "'quarter_millis' not found in bar")
			self._process_rhythm_sequence(bar["quarter_millis"], bar["sequence"])

		path=self.options["qef"]
		try:
			execfile(path, self.globals)
			fishguts.raise_if_false("get_rhythm" in self.globals, "'get_rhythm' function not found")
			result=self.globals["get_rhythm"]()
			if pydash.is_dict(result):
				_process_bar(result)
			else:
				pydash.each(result, _process_bar)
			fishguts.raise_if_false(pydash.any_(self.options["rhythm"], lambda item: item["idata"]==0),
				"rhythm sequence - must include at least one reference to the first stream")
		except Exception as exception:
			raise Exception("failed to parse and execute {0} - {1}".format(path, exception))


	def _process_rhythm_sequence(self, quarter_millis, sequence):
		log.write_very_verbose(" - unprocessed rhythm sequence: {0}", sequence)
		rhythm=self.options["rhythm"]=self.options.get("rhythm", [])
		input_count=len(self.file_path_in)
		tracking=[{
			"auto_advance": True,
			"last_item": None
		} for _ in range(input_count)]
		for section in sequence:
			rhythmic_item={
				"millis": quarter_millis,
				"idata": 0,
				"direction": "f"
			}
			# we allow for a dictionary definition or enumerable, by-position configuration
			if pydash.is_dict(section):
				pydash.merge(rhythmic_item, pydash.omit(section, "ratio"))
				if "ratio" in section:
					rhythmic_item["millis"]*=section["ratio"]
				if "offset" in section:
					rhythmic_item["offset"]*=quarter_millis
				if "advance" in section:
					rhythmic_item["advance"]*=quarter_millis
			else:
				fishguts.raise_if_false(pydash.is_iterable(section) and 2<=len(section)<=4,
					"-qe - each section must be iterable and contain 2 - 4 elements")
				rhythmic_item["millis"]*=section[0]
				rhythmic_item["idata"]=section[1]
				if len(section)==4:
					rhythmic_item["advance"]=section[2]*quarter_millis
					rhythmic_item["direction"]=section[3]
				elif len(section)==3:
					if pydash.is_string(section[2]):
						rhythmic_item["direction"]=section[2]
					else:
						rhythmic_item["advance"]=section[2]*quarter_millis
			### section validation
			fishguts.raise_if_false(rhythmic_item["millis"]>=0, "rhythm item - 'ratio' may not fall below 0. 'offset' may be your man")
			fishguts.raise_if_false(0<=rhythmic_item["idata"]<input_count, "rhythm item - 'idata' must be less than input count")
			fishguts.raise_if_false(rhythmic_item["direction"] in "fb", "rhythm item - 'direction' must be unspecified, 'f' or 'b'")
			### post validation refinement
			# attack/decay we can convert these guys to milliseconds but not samples at this level because the files on which the sample
			# info will come has not been loaded yet. The translation from millis to samples will happen when the sequence is built.
			if "attack" in rhythmic_item:
				rhythmic_item["attack"]*=quarter_millis
			if "decay" in rhythmic_item:
				rhythmic_item["decay"]*=quarter_millis
			tracking[rhythmic_item["idata"]]["last_item"]=rhythmic_item
			if "advance" in rhythmic_item:
				tracking[rhythmic_item["idata"]]["auto_advance"]=False
			rhythm.append(rhythmic_item)
		### final processing
		# per input - if we have not found an advance property then we assume that this section is meant
		# to be moved forward by the bar-length/quarter_millis
		for advance in filter(lambda tracker: tracker["auto_advance"] and tracker["last_item"], tracking):
			advance["last_item"]["advance"]=quarter_millis


	def _load_files(self):
		"""
		Loads each file in self.file_path_in and stores results in self.datas
		@return: None
		"""
		for i in range(len(self.file_path_in)):
			log.write_verbose("loading {0}:", os.path.relpath(self.file_path_in[i]))
			self.datas.append(afile.read(self.file_path_in[i]))
			log.write_verbose(" - sample_rate={0}, sample_count={1}", self.datas[i][0], len(self.datas[i][1]))


	def _config_instrument(self):
		for i in range(len(self.datas)):
			if "a" in self.options:
				self.attack[i]=audio.millis_to_samples(self.datas[i][0], float(self.options["a"]))
			if "d" in self.options:
				self.decay[i]=audio.millis_to_samples(self.datas[i][0], float(self.options["d"]))


	def _config_sequence(self):
		"""
		Finds zero crossings and configures mix sequence
		@return: None
		"""
		log.write_verbose("configuring sequence:")
		if "qe" in self.options or "qef" in self.options:
			self._config_rhythmic_sequence()
		else:
			self._config_interleaved_sequence()


	def _config_rhythmic_sequence(self):
		"""
		Configures sequence as per configuration parsed in option["qe"]
		@return: None
		"""
		rhythm=self.options["rhythm"]
		parts=map(lambda i: {
			"idata": i,
			"rhythm": [],
			"result": None,
			"generator": None
		}, range(len(self.datas)))
		# break our rhythmic sequence up into one per each data stream
		for rhythm_item in rhythm:
			part=parts[rhythm_item["idata"]]
			data=self.datas[rhythm_item["idata"]]
			part["rhythm"].append({
				"length": audio.millis_to_samples(data[0], rhythm_item["millis"]),
				"advance": audio.millis_to_samples(data[0], rhythm_item.get("advance", 0)),
				"offset": audio.millis_to_samples(data[0], rhythm_item.get("offset", 0))
			})
		# generate the crossings for the two bars
		for part in parts:
			if len(part["rhythm"])>0:
				idata=part["idata"]
				part["result"]=ainfo.get_rythmic_zero_crossings(self.datas[idata][1], (part, ))
				# the bar attached to our first audio stream we cycle through once. all others we loop around until the first is exhausted
				part["generator"]=iter(part["result"]["flat"]) if idata==0 else fishguts.iloop(part["result"]["flat"])
				log.write_verbose(" - configuration {0}: crossing_count={1}", idata, len(part["result"]["flat"]))
		# the results should be arranged as they are sequenced in our rhythm. Wwe will keep going around and
		# around until we have reached the end of our first stream
		irhythm_items=fishguts.iloop(rhythm)
		try:
			while True:
				rhythm_item=irhythm_items.next()
				idata=rhythm_item["idata"]
				data=self.datas[idata]
				section=parts[idata]["generator"].next()
				self.sequences.append({
					"data": data,
					"attack": audio.millis_to_samples(data[0], rhythm_item["attack"]) if "attack" in rhythm_item else self.attack[idata],
					"decay": audio.millis_to_samples(data[0], rhythm_item["decay"]) if "decay" in rhythm_item else self.decay[idata],
					"direction": rhythm_item["direction"],
					"isample1": section["start"],
					"isample2": section["start"]+section["length"]
				})
		except StopIteration:
			pass


	def _config_interleaved_sequence(self):
		"""
		Configures sequence of alternating A/B sequences
		@return: None
		"""
		crossings=[]
		directions=self.options["p"]
		# translate each option into a sequence of sample intervals
		for i in range(len(self.datas)):
			# now that we have the sample rate, translate intervals that were specified in units of time
			if "d%d"%(i+1) in self.options:
				value=len(self.datas[i][1])/float(self.options["d%d"%(i+1)])
				intervals=(max(1, int(value)),)
			elif "I%d"%(i+1) in self.options:
				value=self._eval(self.options["I%d"%(i+1)]) if pydash.is_string(self.options["I%d"%(i+1)]) else self.options["I%d"%(i+1)]
				milli_list=value if pydash.is_iterable(value) else (value,)
				intervals=tuple(map(lambda millis:max(1, audio.millis_to_samples(self.datas[i][0], millis)), milli_list))
			elif "i%d"%(i+1) in self.options:
				value=float(self.options["i%d"%(i+1)])
				intervals=(max(1, int(value)),)
			else:
				raise Exception("missing option -d{0} or -i{0} or -I{0}".format(i+1))
			crossings.append(ainfo.get_zero_crossings(self.datas[i][1], intervals))
			log.write_verbose(" - configuration {0}: sample_interval={1}, crossing_count={1}", i, intervals, len(crossings[i]))
		# interleave our intervals into an A|B|A|B playback sequence
		for icrossing_a in xrange(0, len(crossings[0])-1):
			# A sequence
			self.sequences.append({
				"data": self.datas[0],
				"attack": self.attack[0],
				"decay": self.decay[0],
				"direction": directions[(icrossing_a*2)%len(directions)],
				"isample1": abs(crossings[0][icrossing_a]),
				"isample2": abs(crossings[0][icrossing_a+1])
			})
			# B sequence
			icrossing_b=icrossing_a%(len(crossings[1])-1)
			self.sequences.append({
				"data": self.datas[1],
				"attack": self.attack[1],
				"decay": self.decay[1],
				"direction": directions[(icrossing_a*2+1)%len(directions)],
				"isample1": abs(crossings[1][icrossing_b]),
				"isample2": abs(crossings[1][icrossing_b+1])
			})

	def _mix(self):
		def _scale(sample_offset, scalar):
			sample_end=sample_offset+len(scalar)
			result[sample_offset:sample_end]=(result[sample_offset:sample_end]*scalar).astype(result.dtype)

		# start with a size of the first audio file (we know it will be at least this long) and we will increase as necessary
		result=numpy.empty(len(self.datas[0][1]), dtype=numpy.int16)
		iresult=0
		for sequence in self.sequences:
			samples=sequence["data"][1]
			isample1=sequence["isample1"]
			isample2=sequence["isample2"]
			sample_count=isample2-isample1
			# resize if this will exceed our bounds
			if iresult+sample_count>=len(result):
				result=numpy.resize(result, len(result)*2)
			if sequence["direction"]=="b":
				# note: going forward we capture isample1 up until isample2. In reverse we want to include isample2-1 to and including isample1
				result[iresult:iresult+sample_count]=samples[isample2-1:(isample1-1 if isample1-1>=0 else None):-1]
			else:
				result[iresult:iresult+sample_count]=samples[isample1:isample2]
			if sequence["attack"]>0:
				sattack=min(sequence["attack"], sample_count)
				_scale(iresult, numpy.arange(0, sattack)/float(sattack))
			if sequence["decay"]>0:
				sdecay=min(sequence["decay"], sample_count)
				_scale(iresult+sample_count-sdecay, numpy.arange(sdecay, 0, -1)/float(sdecay))
			iresult+=sample_count
		return result[0:iresult]


# --- Action ---#
captain=Commander()
try:
	captain.setup()
except Exception as exception:
	log.write_error(exception)
	captain.usage()
	exit(1)

try:
	captain.execute()
except Exception as exception:
	log.write_failed(exception)
	traceback.print_exc()
	exit(1)
