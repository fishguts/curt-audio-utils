#!/usr/bin/env python

"""
Breaks midi file up into tracks by melodic line
created: 11/5/2016
@author: curt
"""

from __future__ import division

import os
import traceback
from os import path

import midi
import pydash
import sys

import fishguts
from fishguts import cmdline
from fishguts import log


class Commander:
	def __init__(self):
		self.dir_app=os.path.dirname(os.path.realpath(__file__))
		self.options=None
		self.config=None
		self.path_source=None
		self.path_target=None
		self.midi_source=None
		self.midi_target=None
		self.notes_source=None
		self.notes_track_map={}

	# --- public interface---#
	@staticmethod
	def usage():
		print "usage: midistrate.py [-options] <source.mid> [target.mid]"
		print "  -v: verbose"
		print "  -vv: very verbose"
		print "  -c: configuration file. Defaults to config/midistrate_conf.json"
		print "  -a: algorithm: score"
		print "  -s.d: score.distance: 0-1"
		print "  -s.m: score.melody: 0-1"
		print "  -s.t: score.track: 0-1"
		print "  -s.v: score.velocity: 0-1"

	def setup(self):
		"""
		Reads the command line and setup environment variables
		@raise: if parsing fails
		"""
		overrides=(
			# (option, path, optional validator)
			("a", "algorithm", None, lambda v: pydash.contains(["score"], v)),
			("s.d", "score.distance", float, lambda v: 0<=v<=1),
			("s.m", "score.melody", float, lambda v: 0<=v<=1),
			("s.t", "score.track", float, lambda v: 0<=v<=1),
			("s.v", "score.velocity", float, lambda v: 0<=v<=1)
		)
		self.options, position=cmdline.parse(1, ["v", "vv"], ["c"]+[override[0] for override in overrides])
		log.set_verbose("v" in self.options, "vv" in self.options)
		log.write_verbose("processing command line")
		self.path_source=position[0]
		if len(position)>1:
			self.path_target=position[1]
		else:
			self.path_target="{0}-midistrate{1}".format(*path.splitext(self.path_source))
		self.config=fishguts.path_to_json(os.path.join(self.dir_app, "./config/midistrate_conf.json"))
		if "c" in self.options:
			pydash.merge(self.config, fishguts.path_to_json(self.options["c"]))
		# apply option overrides directly to self.config
		for override in overrides:
			if override[0] in self.options:
				value=self.options[override[0]]
				if override[2] is not None:
					value=override[2](value)
				self.config=pydash.set_(self.config, override[1], value)
				# validation
				if override[3] is not None:
					fishguts.raise_if_false(override[3](value), "-{0}: invalid value '{1}'".format(override[0], value))


	def execute(self):
		"""
		Sequences operations and then mixes them according to env configuration
		"""
		self._load_files()
		self._generate_target()
		self._write_results()

	# --- private api ---#
	def _load_files(self):
		"""
		Loads midi file and all dependent audio files
		@return: None
		"""
		log.write_verbose("loading {0}:", os.path.relpath(self.path_source))
		self.midi_source=midi.read_midifile(self.path_source)

	def _write_results(self):
		log.write_verbose("writing {0}", os.path.relpath(self.path_target))
		midi.write_midifile(self.path_target, self.midi_target)

	def _generate_target(self):
		self._create_target_stub()
		self._merge_source_notes()
		self._notes_to_target()

	def _create_target_stub(self):
		"""
		create a midi_target container and retain some meta data
		"""
		track_meta=midi.Track()
		self.midi_target=midi.Pattern(resolution=self.midi_source.resolution, format=self.midi_source.format)
		# let's retain the first track's meta data. we are not going to modify it hence we don't need to clone anything.
		for event in fishguts.get(self.midi_source, 0, []):
			if not isinstance(event, midi.NoteEvent):
				track_meta.append(event)
		if len(track_meta)>0:
			self.midi_target.append(track_meta)


	def _merge_source_notes(self):
		"""
		merges all of the source tracks note events into a single track and returns results
		@return: array of events
		"""
		self.notes_source=midi.Track()
		for track_source in self.midi_source:
			for event in track_source:
				if isinstance(event, midi.NoteEvent):
					self.notes_source.insert_event(event)
					self.notes_track_map[id(event)]=track_source

	def _notes_to_target(self):
		track_name_index=len(self.midi_target)+1
		while len(self.notes_source)>0:
			self.midi_target.append(self._generate_track("Part {0}".format(track_name_index)))
			track_name_index+=1

	def _generate_track(self, name):
		"""
		Generates a track by pulling out a thread of melody that ranks high with our match algorithm
		@param name: name of track
		@return: track
		"""
		note_prev=self.notes_source.pop(0)
		track=midi.Track([midi.TrackNameEvent(text=name), note_prev])
		while note_prev is not None:
			winner={
				"event": None,
				"index": 0,
				"score": 0-sys.maxint
			}
			index_from=self.notes_source.next_event_index(note_prev.offset+int(note_prev.duration*self.config["minDistance"]))
			if index_from>-1:
				max_offset=note_prev.offset+note_prev.duration*self.config["maxDistance"]
				for index in range(index_from, len(self.notes_source)):
					score=self._note_to_score(note_prev, self.notes_source[index], max_offset)
					if score>winner["score"]:
						winner["score"]=score
						winner["index"]=index
						winner["event"]=self.notes_source[index]
					# we process this after so we pick up something. Worst case it's the next note way down the line.
					if self.notes_source[index].offset>=max_offset:
						break
				track.insert_event(self.notes_source.pop(winner["index"]))
			note_prev=winner["event"]
		return track

	def _note_to_score(self, source, target, max_offset):
		score=self.config["score"]["velocity"]*(127-abs(source.velocity-target.velocity))/127.0
		score+=self.config["score"]["melody"]*(127-abs(source.pitch-target.pitch))/127.0
		score+=self.config["score"]["distance"]*(max_offset-abs(source.offset+source.duration-target.offset))/max_offset
		if self.notes_track_map[id(source)]==self.notes_track_map[id(target)]:
			score+=self.config["score"]["track"]
		return score


# --- Action ---#
captain=Commander()
try:
	captain.setup()
except Exception as exception:
	log.write_error(exception)
	captain.usage()
	exit(1)

try:
	captain.execute()
except Exception as exception:
	log.write_failed(exception)
	traceback.print_exc()
	exit(1)
