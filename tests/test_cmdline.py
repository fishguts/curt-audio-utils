"""
created: 5/10/15
@author: curt
"""

import sys
import unittest
from fishguts import cmdline


class TestCmdLine(unittest.TestCase):
	def test_too_few_params(self):
		sys.argv=["app"]
		with self.assertRaises(Exception):
			cmdline.parse(1)

	def test_unknown_mono_option(self):
		sys.argv=["app", "-a"]
		with self.assertRaises(Exception):
			cmdline.parse()

	def test_unknown_paired_option(self):
		sys.argv=["app", "-a", "b"]
		with self.assertRaises(Exception):
			cmdline.parse()

	def test_single_position(self):
		sys.argv=["app", "arg"]
		options,position=cmdline.parse(1)
		self.assertEquals(len(position), 1)
		self.assertEquals(position[0], "arg")

	def test_known_mono_option(self):
		sys.argv=["app", "-a"]
		options,position=cmdline.parse(0, {"a"})
		self.assertEquals(options["a"], True)

	def test_known_paired_option(self):
		sys.argv=["app", "-a", "b"]
		options,position=cmdline.parse(0, None, {"a"})
		self.assertEquals(options["a"], "b")

