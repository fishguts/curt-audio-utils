"""
created: 7/9/16
@author: curt
"""

import numpy
import unittest

import pydash

from fishguts import audio


class TestTimeConversion(unittest.TestCase):
	def test_samples_to_seconds(self):
		self.assertEqual(audio.samples_to_seconds(10, 0), 0.0)
		self.assertEqual(audio.samples_to_seconds(10, 5), 0.5)
		self.assertEqual(audio.samples_to_seconds(10, 10), 1.0)

	def test_samples_to_millis(self):
		self.assertEqual(audio.samples_to_millis(10, 0), 0)
		self.assertEqual(audio.samples_to_millis(10, 5), 500)
		self.assertEqual(audio.samples_to_millis(10, 10), 1000)

	def test_seconds_to_samples(self):
		self.assertEqual(audio.seconds_to_samples(10, 0.0), 0)
		self.assertEqual(audio.seconds_to_samples(10, 0.5), 5)
		self.assertEqual(audio.seconds_to_samples(10, 1.0), 10)

	def test_millis_to_samples(self):
		self.assertEqual(audio.millis_to_samples(10, 0), 0)
		self.assertEqual(audio.millis_to_samples(10, 500), 5)
		self.assertEqual(audio.millis_to_samples(10, 1000), 10)

class TestCopySamples(unittest.TestCase):
	def test_empty_buffer(self):
		samples=numpy.array([])
		self.assertRaises(Exception, pydash.partial(audio.copy_samples, samples))

	def test_full_copy(self):
		samples=numpy.linspace(0, 10, 10)
		result=audio.copy_samples(samples)
		self.assertTrue(numpy.array_equal(samples, result))

	def test_underflow(self):
		samples=numpy.linspace(0, 10, 10)
		result=audio.copy_samples(samples, ioffset=0, count=5)
		self.assertTrue(numpy.array_equal(result, samples[:5]))
		result=audio.copy_samples(samples, ioffset=1, count=5)
		self.assertTrue(numpy.array_equal(result, samples[1:6]))
		result=audio.copy_samples(samples, ioffset=9, count=1)
		self.assertTrue(numpy.array_equal(result, samples[-1:]))

	def test_overflow_no_algorithm(self):
		samples=numpy.linspace(0, 10, 10)
		result=audio.copy_samples(samples, ioffset=0, count=15)
		self.assertTrue(numpy.array_equal(result, samples))
		result=audio.copy_samples(samples, ioffset=9, count=10)
		self.assertTrue(numpy.array_equal(result, samples[-1:]))

	def test_overflow_with_zero(self):
		samples=numpy.linspace(0, 10, 10)
		result=audio.copy_samples(samples, ioffset=0, count=15, algorithm="zero")
		self.assertTrue(numpy.array_equal(result[:10], samples))
		self.assertTrue(numpy.array_equal(result[10:15], numpy.zeros((5,))))
		result=audio.copy_samples(samples, ioffset=5, count=10, algorithm="zero")
		self.assertTrue(numpy.array_equal(result[:5], samples[5:10]))
		self.assertTrue(numpy.array_equal(result[5:10], numpy.zeros((5,))))

	def test_overflow_with_wrap(self):
		samples=numpy.linspace(0, 10, 10)
		result=audio.copy_samples(samples, ioffset=0, count=15, algorithm="wrap")
		self.assertTrue(numpy.array_equal(result[:10], samples))
		self.assertTrue(numpy.array_equal(result[10:15], result[:5]))
		result=audio.copy_samples(samples, ioffset=5, count=10, algorithm="wrap")
		self.assertTrue(numpy.array_equal(result[:5], samples[5:10]))
		self.assertTrue(numpy.array_equal(result[5:10], samples[:5]))

if __name__=='__main__':
	unittest.main()
