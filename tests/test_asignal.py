"""
created: 5/25/15
@author: curt
"""

import numpy as np
from unittest import TestCase
from fishguts.audio import asignal


class TestCreateSinNormal(TestCase):
	def test_seconds_1(self):
		a=asignal.create_sin_normal(10, 1, tlen=1)
		self.assertEqual(len(a), 10)
		self.assertTrue(np.array_equal(a, np.sin(np.arange(0, 2*np.pi, 2*np.pi/10))))

	def test_seconds_2(self):
		a=asignal.create_sin_normal(10, 1, tlen=2)
		self.assertEqual(len(a), 20)
		self.assertTrue(np.array_equal(a, np.sin(np.arange(0, 4*np.pi, 2*np.pi/10))))

	def test_samples_5(self):
		a=asignal.create_sin_normal(10, 1, slen=5)
		self.assertEqual(len(a), 5)
		self.assertTrue(np.array_equal(a, np.sin(np.arange(0, np.pi, 2*np.pi/10))))

	def test_samples_10(self):
		a=asignal.create_sin_normal(10, 1, slen=10)
		self.assertEqual(len(a), 10)
		self.assertTrue(np.array_equal(a, np.sin(np.arange(0, 2*np.pi, 2*np.pi/10))))

