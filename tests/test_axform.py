"""
created: 5/24/15
@author: curt
"""
import unittest
import numpy

from fishguts.audio import axform


class TestModulate(unittest.TestCase):
	def test_matched_buffers(self):
		a=numpy.array([4, 4, 4, 4, 4])
		b=numpy.array([0, 0.25, 0.5, 0.75, 1])
		c=axform.modulate(a, b)
		self.assertEquals(c.dtype, a.dtype)
		self.assertTrue(numpy.array_equal(c, numpy.array([0, 1, 2, 3, 4])))

	def test_undersized_scalar(self):
		a=numpy.array([4, 4, 4, 4])
		b=numpy.array([0, 0.25 ])
		c=axform.modulate(a, b)
		self.assertEquals(c.dtype, a.dtype)
		self.assertTrue(numpy.array_equal(c, numpy.array([0, 1, 0, 1])))

	def test_oversized_scalar(self):
		a=numpy.array([4, 4 ])
		b=numpy.array([0, 0.25, 0.5])
		c=axform.modulate(a, b)
		self.assertEquals(c.dtype, a.dtype)
		self.assertTrue(numpy.array_equal(c, numpy.array([0, 1])))

	def test_rate_sub(self):
		a=numpy.array([3, 3, 3, 3])
		b=numpy.array([0, 1.0])
		c=axform.modulate(a, b, 1/3.0)
		self.assertEquals(c.dtype, a.dtype)
		self.assertTrue(numpy.array_equal(c, numpy.array([0, 1, 2, 3])))

	def test_rate_super(self):
		a=numpy.array([10, 10, 10])
		b=numpy.array([0, 0.1, 0.2, 0.3, 0.4, 0.5])
		c=axform.modulate(a, b, 2.0)
		self.assertEquals(c.dtype, a.dtype)
		self.assertTrue(numpy.array_equal(c, numpy.array([0, 2, 4])))

