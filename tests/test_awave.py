"""
created: 5/23/15
@author: curt
"""

import numpy
import unittest
from fishguts.audio import afile

class TestAudioWave(unittest.TestCase):
	def test_read_file_success(self):
		srate, samples=afile.read("./data/test/da_short01.wav")
		self.assertEqual(srate, 22050)
		self.assertTrue(type(samples) is numpy.ndarray)

	def test_read_file_failure(self):
		self.assertRaises(Exception, afile.read, "./wavc.py")
