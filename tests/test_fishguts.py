"""
created: 10/10/15
@author: curt
"""

from unittest import TestCase

import fishguts

class TestFishguts(TestCase):
	def test_iloop(self):
		sequence=[1,2,3]
		loop=fishguts.iloop(sequence)
		for i in range(6):
			self.assertEqual(loop.next(), sequence[i%3])

	def test_ibounce(self):
		sequence=[1,2,3]
		expected=[1,2,3,2]*3
		loop=fishguts.ibounce(sequence)
		for i in range(len(expected)):
			self.assertEqual(loop.next(), expected[i])

	def test_ialternate(self):
		sequence=[1,2,3,4]
		expected=[1,4,2,3,3,2,4,1]*2
		loop=fishguts.ialternate(sequence)
		for i in range(len(expected)):
			self.assertEqual(loop.next(), expected[i])

	def test_ialternate_last(self):
		sequence=[1,2,3,4]
		expected=[4,1,3,2,2,3,1,4]*2
		loop=fishguts.ialternate(sequence, offset="last")
		for i in range(len(expected)):
			self.assertEqual(loop.next(), expected[i])

	def test_irandom(self):
		sequence=range(1, 10)
		loop=fishguts.irandom(sequence)
		for i in range(10):
			self.assertTrue(sequence[0]<=loop.next()<=sequence[-1])

	def test_isin(self):
		expected=[0, 1, 0, -1]*2
		loop=fishguts.isin(4)
		for i in range(8):
			self.assertAlmostEqual(loop.next(), expected[i])

	def test_isin_range(self):
		expected=[3, 2, 3, 4]*2
		loop=fishguts.isin(4, vfrom=4, vto=2)
		for i in range(8):
			self.assertAlmostEqual(loop.next(), expected[i])

	def test_isin_count(self):
		result=[]
		for value in fishguts.isin(4, count=2):
			result.append(value)
		self.assertEqual(result, [0, 1])

	def test_iexp_count(self):
		result=[]
		for value in fishguts.iexp(2, count=2):
			result.append(value)
		self.assertEqual(result, [0, 1])

	def test_scrub(self):
		self.assertEqual(fishguts.scrub(None), None)
		self.assertEqual(fishguts.scrub(1), 1)
		self.assertEqual(fishguts.scrub("x"), "x")
		self.assertEqual(fishguts.scrub({}), {})
		self.assertEqual(fishguts.scrub({"a":None}), {})
		self.assertEqual(fishguts.scrub({"a":1}), {"a":1})
		self.assertEqual(fishguts.scrub({"a":{"b":None}}, False), {"a":{"b":None}})
		self.assertEqual(fishguts.scrub({"a":{"b":None}}, True), {})
		self.assertEqual(fishguts.scrub({"a":{}}, empty=(None,)), {"a":{}})
